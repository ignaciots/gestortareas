package org.ignaciots.gestortareas;

import org.ignaciots.gestortareas.logica.filtro.Filtro;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroCategoria;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroCualquiera;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroDescripcion;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroEstado;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroFecha;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroHora;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroMayorQue;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroMenorQue;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNombre;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNot;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNotificable;
import org.ignaciots.gestortareas.model.Categoria;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests para los {@link Filtro} de tareas.
 * @author Ignacio Torre Suárez
 */
public class FiltroUnitTest {

    private static final String[] FECHAS = {"2018-09-11 15:14:00","2018-10-12 12:40:00","2020-09-30 09:55:00","2018-09-01 19:25:00","2021-01-24 11:15:00",
            "2019-10-21 22:15:00","2019-12-18 04:57:00","2018-11-06 06:22:00","2018-06-13 07:51:00","2019-10-11 20:23:00",
            "2020-07-19 00:37:00","2019-04-07 03:04:00","2020-11-23 21:47:00","2019-11-20 17:24:00","2020-11-01 05:59:00",
            "2019-01-20 18:37:00","2020-05-25 20:46:00","2020-11-06 03:51:00","2020-04-13 21:20:00","2019-07-23 08:32:00",
            "2018-11-30 01:50:00","2020-09-26 00:22:00","2020-08-14 01:32:00","2018-08-01 00:48:00","2018-08-24 04:30:00",
            "2018-05-16 03:03:00","2021-01-10 07:32:00","2018-10-23 22:48:00","2020-06-20 08:14:00","2018-04-19 13:38:00",
            "2020-12-18 12:12:00","2019-09-21 12:59:00","2020-09-25 11:06:00","2019-09-07 22:25:00","2020-11-26 01:33:00",
            "2019-09-19 21:03:00","2019-01-26 05:54:00","2018-04-01 19:16:00","2019-08-20 21:13:00","2019-04-13 06:57:00",
            "2018-11-18 16:55:00","2018-11-22 19:42:00","2019-09-23 00:15:00","2021-01-13 18:58:00","2019-01-17 13:07:00",
            "2019-01-13 19:15:00","2020-09-18 06:02:00","2020-12-18 13:49:00","2020-07-17 22:58:00","2020-09-14 08:09:00",
            "2020-03-23 19:15:00","2020-03-23 19:15:00","2020-03-23 19:16:00"};

    private static final Estado[] ESTADOS = {Estado.COMPLETADO,Estado.COMPLETADO,Estado.SIN_COMPLETAR,Estado.COMPLETADO,Estado.VENCIDO,
            Estado.VENCIDO,Estado.COMPLETADO,Estado.SIN_COMPLETAR,Estado.SIN_COMPLETAR,Estado.SIN_COMPLETAR,
            Estado.COMPLETADO,Estado.VENCIDO,Estado.SIN_COMPLETAR,Estado.SIN_COMPLETAR,Estado.VENCIDO,
            Estado.SIN_COMPLETAR,Estado.VENCIDO,Estado.COMPLETADO,Estado.VENCIDO,Estado.COMPLETADO,
            Estado.SIN_COMPLETAR,Estado.SIN_COMPLETAR,Estado.VENCIDO,Estado.COMPLETADO,Estado.SIN_COMPLETAR,
            Estado.VENCIDO,Estado.SIN_COMPLETAR,Estado.SIN_COMPLETAR,Estado.COMPLETADO,Estado.SIN_COMPLETAR,
            Estado.SIN_COMPLETAR,Estado.SIN_COMPLETAR,Estado.COMPLETADO,Estado.VENCIDO,Estado.VENCIDO,
            Estado.COMPLETADO,Estado.SIN_COMPLETAR,Estado.VENCIDO,Estado.SIN_COMPLETAR,Estado.COMPLETADO,
            Estado.SIN_COMPLETAR,Estado.VENCIDO,Estado.VENCIDO,Estado.VENCIDO,Estado.COMPLETADO,
            Estado.VENCIDO,Estado.COMPLETADO,Estado.VENCIDO,Estado.COMPLETADO,Estado.COMPLETADO,
            Estado.VENCIDO,Estado.COMPLETADO,Estado.VENCIDO};

    private static final Notificable[] NOTIFICACIONES = {Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,
            Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,
            Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,
            Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,
            Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NO_NOTIFICABLE,
            Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NO_NOTIFICABLE,
            Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,
            Notificable.NO_NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,
            Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,
            Notificable.NO_NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,
            Notificable.NOTIFICABLE,Notificable.NOTIFICABLE,Notificable.NO_NOTIFICABLE,};


    private static final Categoria[] CATEGORIAS = {new Categoria(0, "Compras"), new Categoria(1, "Tareas"), new Categoria(2, "Deportes"),
            new Categoria(3, "Otros"), null};

    private static final int[] INDICES_CATEGORIA = {2,2,1,2,0,
            0,2,2,2,0,
            0,0,2,1,1,
            2,1,0,2,2,
            2,2,2,0,0,
            1,0,1,1,1,
            0,1,1,1,2,
            0,0,0,2,0,
            0,2,2,0,0,
            2,1,1,1,1,
            3,4,4};

    private static final int NUMERO_TAREAS = 53;

    static List<Tarea> tareas = new ArrayList<>();

    @BeforeClass
    public static void setUpTareas() throws ParseException {
        for (int i = 1; i<= NUMERO_TAREAS; i++) {
            tareas.add(new Tarea(i, "T" + i, "Tarea " + i, Tarea.stringToCalendar(FECHAS[i-1]), ESTADOS[i-1], NOTIFICACIONES[i-1], CATEGORIAS[INDICES_CATEGORIA[i-1]]));
        }
    }

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void filtroNombreTest() {
        // Filtrar por tareas cuyo nombre contenga el texto "Cero". El resultado debería ser 0.
        Filtro filtroNombre = new FiltroNombre("Cero");
        List<Tarea> tareasFiltradas = filtroNombre.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre contenga el texto "Compra". El resultado debería ser 1.
        filtroNombre = new FiltroNombre("T23");
        tareasFiltradas = filtroNombre.filtrarTareas(tareas);
        assertEquals(1, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre contenga el texto "T1". El resultado debería ser 11
        // (Tareas 1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19).
        filtroNombre = new FiltroNombre("T1");
        tareasFiltradas = filtroNombre.filtrarTareas(tareas);
        assertEquals(11, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre contenga el texto "T". El resultado debería ser el número total de tareas, 53..
        filtroNombre = new FiltroNombre("T");
        tareasFiltradas = filtroNombre.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());
    }

    @Test
    public void filtroDescripcionTest() {
        // Filtrar por tareas cuya descripción contenga el texto "Prueba". El resultado debería ser 0.
        Filtro filtroDescripcion = new FiltroDescripcion("Prueba");
        List<Tarea> tareasFiltradas = filtroDescripcion.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya descripción contenga el texto "Tarea 50". El resultado debería ser 1.
        filtroDescripcion = new FiltroDescripcion("Tarea 50");
        tareasFiltradas = filtroDescripcion.filtrarTareas(tareas);
        assertEquals(1, tareasFiltradas.size());

        // Filtrar por tareas cuya descripción contenga el texto "Tarea 3". El resultado debería ser 11.
        // (Tareas 3, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39).
        filtroDescripcion = new FiltroDescripcion("Tarea 3");
        tareasFiltradas = filtroDescripcion.filtrarTareas(tareas);
        assertEquals(11, tareasFiltradas.size());

        // Filtrar por tareas cuya descripción contenga el texto "area". El resultado debería ser el número total de tareas, 53..
        filtroDescripcion = new FiltroDescripcion("area");
        tareasFiltradas = filtroDescripcion.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());
    }

    @Test
    public void filtroCategoriaTest() {
        // Filtrar por tareas cuyo nombre de categoría contenga el texto "23". El resultado debería ser 0.
        Filtro filtroCategoria = new FiltroCategoria("23");
        List<Tarea> tareasFiltradas = filtroCategoria.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre de categoría contenga el texto "Otro". El resultado debería ser 1.
        filtroCategoria = new FiltroCategoria("Otro");
        tareasFiltradas = filtroCategoria.filtrarTareas(tareas);
        assertEquals(1, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre de categoría contenga el texto "Compras". El resultado debería ser 17.
        filtroCategoria = new FiltroCategoria("Compras");
        tareasFiltradas = filtroCategoria.filtrarTareas(tareas);
        assertEquals(17, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre de categoría contenga el texto "s". El resultado debería ser 51.
        filtroCategoria = new FiltroCategoria("s");
        tareasFiltradas = filtroCategoria.filtrarTareas(tareas);
        assertEquals(51, tareasFiltradas.size());
    }

    @Test
    public void filtroEstadoTest() {
        // Filtrar por tareas cuyo estado sea igual a VENCIDO. El resultado debería ser 18.
        Filtro filtroEstado = new FiltroEstado(Estado.VENCIDO);
        List<Tarea> tareasFiltradas = filtroEstado.filtrarTareas(tareas);
        assertEquals(18, tareasFiltradas.size());

        // Filtrar por tareas cuyo estado sea igual a SIN_COMPLETAR. El resultado debería ser 18.
        filtroEstado = new FiltroEstado(Estado.SIN_COMPLETAR);
        tareasFiltradas = filtroEstado.filtrarTareas(tareas);
        assertEquals(18, tareasFiltradas.size());

        // Filtrar por tareas cuyo estado sea igual a COMPLETADO. El resultado debería ser 17.
        filtroEstado = new FiltroEstado(Estado.COMPLETADO);
        tareasFiltradas = filtroEstado.filtrarTareas(tareas);
        assertEquals(17, tareasFiltradas.size());
    }

    @Test
    public void filtroNotificableTest() {
        // Filtrar por tareas cuyo tipo de notificaciones sea igual a NOTIFICABLE. El resultado debería ser 18.
        Filtro filtroNotificable = new FiltroNotificable(Notificable.NOTIFICABLE);
        List<Tarea> tareasFiltradas = filtroNotificable.filtrarTareas(tareas);
        assertEquals(33, tareasFiltradas.size());

        // Filtrar por tareas cuyo tipo de notificaciones sea igual a NO_NOTIFICABLE. El resultado debería ser 18.
        filtroNotificable = new FiltroNotificable(Notificable.NO_NOTIFICABLE);
        tareasFiltradas = filtroNotificable.filtrarTareas(tareas);
        assertEquals(20, tareasFiltradas.size());
    }

    @Test
    public void filtroFechaTest() throws ParseException {
        // Filtrar por tareas cuya fecha sea igual a 2015-11-03. El resultado debería ser 0.
        Filtro filtroFecha = new FiltroFecha(Tarea.stringToCalendar("2015-11-03 00:00:00"));
        List<Tarea> tareasFiltradas = filtroFecha.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea igual a 2020-09-25. El resultado debería ser 1.
        filtroFecha = new FiltroFecha(Tarea.stringToCalendar("2020-09-25 00:00:00"));
        tareasFiltradas = filtroFecha.filtrarTareas(tareas);
        assertEquals(1, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea igual a 2019-04-07. El resultado debería ser 1.
        filtroFecha = new FiltroFecha(Tarea.stringToCalendar("2019-04-07 03:04:00"));
        tareasFiltradas = filtroFecha.filtrarTareas(tareas);
        assertEquals(1, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea igual a 2020-03-23. El resultado debería ser 3.
        filtroFecha = new FiltroFecha(Tarea.stringToCalendar("2020-03-23 19:15:00"));
        tareasFiltradas = filtroFecha.filtrarTareas(tareas);
        assertEquals(3, tareasFiltradas.size());
    }

    @Test
    public void filtroHoraTest() throws ParseException {
        // Filtrar por tareas cuya hora sea igual a 00:00. El resultado debería ser 0.
        Filtro filtroHora = new FiltroHora(Tarea.stringToCalendar("01-01-2000 00:00:00"));
        List<Tarea> tareasFiltradas = filtroHora.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea igual a 03:51. El resultado debería ser 1.
        filtroHora = new FiltroHora(Tarea.stringToCalendar("01-01-2000 03:51:00"));
        tareasFiltradas = filtroHora.filtrarTareas(tareas);
        assertEquals(1, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea igual a 19:16. El resultado debería ser 1.
        filtroHora = new FiltroHora(Tarea.stringToCalendar("01-01-2000 19:16:00"));
        tareasFiltradas = filtroHora.filtrarTareas(tareas);
        assertEquals(2, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea igual a 19:15. El resultado debería ser 3.
        filtroHora = new FiltroHora(Tarea.stringToCalendar("01-01-2000 19:15:00"));
        tareasFiltradas = filtroHora.filtrarTareas(tareas);
        assertEquals(3, tareasFiltradas.size());
    }

    @Test
    public void filtroCualquieraTest() {

        // Filtrar por tareas con nombre cualquiera. El resultado debería ser 53
        Filtro filtroCualquiera = new FiltroCualquiera(new FiltroNombre(""));
        List<Tarea> tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con descripción cualquiera. El resultado debería ser 53
        filtroCualquiera = new FiltroCualquiera(new FiltroDescripcion("abcd"));
        tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con categoría cualquiera. El resultado debería ser 53
        filtroCualquiera = new FiltroCualquiera(new FiltroCategoria(""));
        tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con estado cualquiera. El resultado debería ser 53
        filtroCualquiera = new FiltroCualquiera(new FiltroEstado(Estado.COMPLETADO));
        tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con notificaciones cualquiera. El resultado debería ser 53
        filtroCualquiera = new FiltroCualquiera(new FiltroNotificable(Notificable.NO_NOTIFICABLE));
        tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con fecha cualquiera. El resultado debería ser 53
        filtroCualquiera = new FiltroCualquiera(new FiltroFecha(Calendar.getInstance()));
        tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con hora cualquiera. El resultado debería ser 53
        filtroCualquiera = new FiltroCualquiera(new FiltroHora(Calendar.getInstance()));
        tareasFiltradas = filtroCualquiera.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());
    }

    @Test
    public void filtroNotTest() throws ParseException {

        // Filtrar por tareas con nombre que no contenga el texto "Prueba". El resultado debería ser 53
        Filtro filtroNot = new FiltroNot(new FiltroNombre("Prueba"));
        List<Tarea> tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas con descripción que no contenga el texto "Tarea". El resultado debería ser 0
        filtroNot = new FiltroNot(new FiltroDescripcion("Tarea"));
        tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuyo nombre de categoría no contenga el texto "Compras". El resultado debería ser 36
        filtroNot = new FiltroNot(new FiltroCategoria("Compras"));
        tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(36, tareasFiltradas.size());

        // Filtrar por tareas cuyo estado no sea VENCIDO. El resultado debería ser 35
        filtroNot = new FiltroNot(new FiltroEstado(Estado.VENCIDO));
        tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(35, tareasFiltradas.size());

        // Filtrar por tareas cuyo notificable no sea NOTIFICABLE. El resultado debería ser 20
        filtroNot = new FiltroNot(new FiltroNotificable(Notificable.NOTIFICABLE));
        tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(20, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha no sea 2020-03-23. El resultado debería ser 50
        filtroNot = new FiltroNot(new FiltroFecha(Tarea.stringToCalendar("2020-03-23 19:15:00")));
        tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(50, tareasFiltradas.size());

        // Filtrar por tareas cuya hora no sea 19:16. El resultado debería ser 51
        filtroNot = new FiltroNot(new FiltroHora(Tarea.stringToCalendar("01-01-2000 19:16:00")));
        tareasFiltradas = filtroNot.filtrarTareas(tareas);
        assertEquals(51, tareasFiltradas.size());
    }

    @Test
    public void filtroMayorQueTest() throws ParseException {

        // Filtrar por tareas cuya fecha sea mayor que 2005-03-02. El resultado debería ser 53
        Filtro filtroMayorQue = new FiltroMayorQue(new FiltroFecha(Tarea.stringToCalendar("2005-03-02 00:00:00")));
        List<Tarea> tareasFiltradas = filtroMayorQue.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea mayor que 2021-01-01. El resultado debería ser 0
        filtroMayorQue = new FiltroMayorQue(new FiltroFecha(Tarea.stringToCalendar("2022-01-01 00:00:00")));
        tareasFiltradas = filtroMayorQue.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea mayor que 2019-06-01. El resultado debería ser 33
        filtroMayorQue = new FiltroMayorQue(new FiltroFecha(Tarea.stringToCalendar("2019-06-01 00:00:00")));
        tareasFiltradas = filtroMayorQue.filtrarTareas(tareas);
        assertEquals(33, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea mayor que 23:55. El resultado debería ser 0
        filtroMayorQue = new FiltroMayorQue(new FiltroHora(Tarea.stringToCalendar("2000-01-01 23:55:00")));
        tareasFiltradas = filtroMayorQue.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea mayor que 00:00. El resultado debería ser 53
        filtroMayorQue = new FiltroMayorQue(new FiltroHora(Tarea.stringToCalendar("2000-01-01 00:00:00")));
        tareasFiltradas = filtroMayorQue.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea mayor que 14:25. El resultado debería ser 22
        filtroMayorQue = new FiltroMayorQue(new FiltroHora(Tarea.stringToCalendar("2000-01-01 14:25:00")));
        tareasFiltradas = filtroMayorQue.filtrarTareas(tareas);
        assertEquals(22, tareasFiltradas.size());
    }

    @Test
    public void filtroMenorQueTest() throws ParseException {

        // Filtrar por tareas cuya fecha sea menor que 1999-05-11. El resultado debería ser 0
        Filtro filtroMenorQue = new FiltroMenorQue(new FiltroFecha(Tarea.stringToCalendar("1999-05-11 00:00:00")));
        List<Tarea> tareasFiltradas = filtroMenorQue.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea menor que 2025-01-01. El resultado debería ser 53
        filtroMenorQue = new FiltroMenorQue(new FiltroFecha(Tarea.stringToCalendar("2025-01-01 00:00:00")));
        tareasFiltradas = filtroMenorQue.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas cuya fecha sea menor que 2020-02-01. El resultado debería ser 33
        filtroMenorQue = new FiltroMenorQue(new FiltroFecha(Tarea.stringToCalendar("2020-02-01 00:00:00")));
        tareasFiltradas = filtroMenorQue.filtrarTareas(tareas);
        assertEquals(30, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea menor que 23:59. El resultado debería ser 53
        filtroMenorQue = new FiltroMenorQue(new FiltroHora(Tarea.stringToCalendar("2000-01-01 23:55:00")));
        tareasFiltradas = filtroMenorQue.filtrarTareas(tareas);
        assertEquals(53, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea menor que 00:00. El resultado debería ser 0
        filtroMenorQue = new FiltroMenorQue(new FiltroHora(Tarea.stringToCalendar("2000-01-01 00:00:00")));
        tareasFiltradas = filtroMenorQue.filtrarTareas(tareas);
        assertEquals(0, tareasFiltradas.size());

        // Filtrar por tareas cuya hora sea menor que 07:43. El resultado debería ser 18
        filtroMenorQue = new FiltroMenorQue(new FiltroHora(Tarea.stringToCalendar("2000-01-01 07:43:00")));
        tareasFiltradas = filtroMenorQue.filtrarTareas(tareas);
        assertEquals(18, tareasFiltradas.size());
    }


}