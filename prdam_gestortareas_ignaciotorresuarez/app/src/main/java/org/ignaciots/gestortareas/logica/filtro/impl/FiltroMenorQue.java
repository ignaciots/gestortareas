package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroDecorator;
import org.ignaciots.gestortareas.logica.filtro.Filtro;

/**
 * Filtro decorator utilizado para filtrar tareas cuyo resultado del filtro haya sido -1 (menor que).
 * @author Ignacio Torre Suárez
 */
public class FiltroMenorQue extends AbstractFiltroDecorator {

    /**
     * Crea un filtro que filtra por resultados menores a 0 del filtro pasado como parámetro.
     * @param filtro El filtro a ejecutar.
     */
    public FiltroMenorQue(Filtro filtro) {
        super(filtro);
    }

    @Override
    protected int doFiltrarEstado(int estado) {
        return estado < 0 ? 0 : -1;
    }
}
