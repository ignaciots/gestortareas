package org.ignaciots.gestortareas.xml;

import org.ignaciots.gestortareas.xml.model.Gestor;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;

/**
 * Clase que se encarga de gestionar la transformación y procesamiento de archivos XML.
 * @author Ignacio Torre Suárez
 */
public class XMLManager {

    /**
     * Transforma un archivo XML en objetos compatibles con la base de datos y el modelo de la aplicación.
     * @param archivoXML La ruta del archivo XML a ser transformado.
     * @return Un objeto de tipo {@link Gestor} que contiene los datos transformados.
     * @throws Exception Lanzada cuando ocurre un error al acceder o procesar el archivo XML y las tareas.
     */
    public Gestor parseXML(File archivoXML) throws Exception {
        Serializer serializer = new Persister();
        return serializer.read(Gestor.class, archivoXML);
    }

    /**
     * Transforma un objeto {@link Gestor} en un XML exportable que contiene sus datos.
     * @param archivoXML La ruta del archivo XML a ser generado.
     * @param gestor El {@link Gestor} ser transformado en archivo XML.
     * @return Un objeto {@link File} que representa el archivo XML creado.
     * @throws Exception Lanzada cuando ocurre un error al acceder o procesar el archivo XML y las tareas.
     */
    public File crearXML(String archivoXML, Gestor gestor) throws Exception {
        Serializer serializer = new Persister();
        File xml = new File(archivoXML);
        serializer.write(gestor, xml);
        return xml;
    }
}
