package org.ignaciots.gestortareas.model;

import androidx.annotation.NonNull;

/**
 * Enumerado para el campo estado utilizado en el modelo de base de datos.
 * SIN_COMPLETAR indica que la tarea aún no ha sido completada.
 * COMPLETADO indica que la tarea ha sido completada.
 * VENCIDO indica que la tarea ya ha vencido (ha expirado).
 */
public enum Estado {
    SIN_COMPLETAR(0, "Sin completar"),
    COMPLETADO(1, "Completado"),
    VENCIDO(2, "Vencido");

    private final int id;
    private final String nombre;

    Estado(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return this.nombre;
    }

    @Override
    @NonNull
    public String toString() {
        return getNombre();
    }
}
