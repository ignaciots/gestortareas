package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltro;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;

/**
 * Filtro utilizado para filtrar {@link Notificable}.
 * @author Ignacio Torre Suárez
 */
public class FiltroNotificable extends AbstractFiltro {

    private final Notificable notificable;

    /**
     * Crea un filtro que filtra por notificables iguales al estado dado.
     * @param notificable El notificable usado en el filtro.
     */
    public FiltroNotificable(Notificable notificable) {
        this.notificable = notificable;
    }

    @Override
    protected int comparar(Tarea tarea) {
        // Usado en los filtros
        if (notificable == null) {
            return 1;
        }
        return notificable.compareTo(tarea.getNotificable());
    }
}
