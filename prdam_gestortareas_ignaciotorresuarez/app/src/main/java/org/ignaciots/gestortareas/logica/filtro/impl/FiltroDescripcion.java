package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroTexto;
import org.ignaciots.gestortareas.model.Tarea;

/**
 * Filtro utilizado para filtrar descripciones.
 * @author Ignacio Torre Suárez
 */
public class FiltroDescripcion extends AbstractFiltroTexto {

    /**
     * Crea un filtro que filtra por descripciones que contienen el texto dado.
     * @param texto El texto usado en el filtro.
     */
    public FiltroDescripcion(String texto) {
        super(texto);
    }

    @Override
    protected String getTextoTarea(Tarea tarea) {
        return tarea.getDescripcion();
    }
}
