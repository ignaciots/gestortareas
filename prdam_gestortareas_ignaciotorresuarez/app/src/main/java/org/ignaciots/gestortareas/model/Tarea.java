package org.ignaciots.gestortareas.model;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Clase que representa a la entidad Tarea utilizada en el modelo de la base de datos.
 * @author Ignacio Torre Suárez
 */
public class Tarea {

    private static final String PATTERN_CALENDAR_TAREA        = "yyyy-MM-dd HH:mm:ss";
    private static final String PATTERN_CALENDAR_PRETTY_TAREA = "dd-MM-yyyy HH:mm";
    private static final String PATTERN_FECHA_PRETTY_TAREA    = "dd-MM-yyyy";
    private static final String PATTERN_DATE_PRETTY_TAREA     = "HH:mm";

    private int id;
    private String nombre;
    private String descripcion;
    private Calendar fechaFin;
    private Estado estado;
    private Notificable notificable;
    private Categoria categoria;


    public Tarea() {}

    public Tarea(int id, String nombre, String descripcion, Calendar fechaFin, Estado estado, Notificable notificable, Categoria categoria) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fechaFin = fechaFin;
        this.estado = estado;
        this.notificable = notificable;
        this.categoria = categoria;
    }

    /**
     * Transforma un texto de fecha en un {@link Calendar} equivalente.
     * @param fechaString El {@link String} a ser transformado.
     * @return El {@link Calendar} equivalente.
     * @throws ParseException Lanzado cuando ocurre un error al convertir la fecha.
     */
    public static Calendar stringToCalendar(String fechaString) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_CALENDAR_TAREA, Locale.US);
        Date fecha = dateFormat.parse(fechaString);
        if (fecha == null) {
            throw new ParseException("Error al convertir fecha: fecha es null", 0);
        }
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(fecha);

        return calendario;
    }

    /**
     * Transforma un {@link Calendar} en una fecha equivalente.
     * @param fechaCalendario El {@link Calendar} a ser transformado.
     * @return Un {@link String} con la fecha equivalente.
     */
    public static String calendarToString(Calendar fechaCalendario) {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_CALENDAR_TAREA, Locale.US);
        return dateFormat.format(fechaCalendario.getTime());
    }

    /**
     * Transforma un {@link Calendar} en una fecha legible para el usuario.
     * @param fechaCalendario El {@link Calendar} a ser transformado.
     * @return Un {@link String} con la fecha equivalente legible para el usuario.
     */
    public static String calendarToPrettyString(Calendar fechaCalendario) {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_CALENDAR_PRETTY_TAREA, Locale.US);
        return  dateFormat.format(fechaCalendario.getTime());
    }

    /**
     * Transforma un {@link Calendar} en una fecha sin hora legible para el usuario.
     * @param fechaCalendario El {@link Calendar} a ser transformado.
     * @return Un {@link String} con la fecha (sin hora) equivalente legible para el usuario.
     */
    public static String calendarDateToPrettyString(Calendar fechaCalendario) {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_FECHA_PRETTY_TAREA, Locale.US);
        return dateFormat.format(fechaCalendario.getTime());
    }

    /**
     * Transforma un {@link Calendar} en una hora legible para el usuario.
     * @param fechaCalendario El {@link Calendar} a ser transformado.
     * @return Un {@link String} con la hora equivalente legible para el usuario.
     */
    public static String calendarTimeToPrettyString(Calendar fechaCalendario) {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_DATE_PRETTY_TAREA, Locale.US);
        return dateFormat.format(fechaCalendario.getTime());
    }

    /**
     * Transforma un {@link Estado} en su INTEGER equivalente en la base de datos.
     * @param estado El {@link Estado} a transformar.
     * @return int que representa al estado transformado.
     */
    public static Estado integerToEstado(int estado) {
        return Estado.values()[estado];
    }

    /**
     * Transforma un {@link Notificable} en su INTEGER equivalente en la base de datos.
     * @param notificable El {@link Notificable} a transformar.
     * @return int que representa al notificable transformado.
     */
    public static Notificable integerToNotificable(int notificable) {
        return Notificable.values()[notificable];
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Calendar getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Calendar fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Notificable getNotificable() {
        return notificable;
    }

    public void setNotificable(Notificable notificable) {
        this.notificable = notificable;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tarea tarea = (Tarea) o;
        return id == tarea.id &&
                nombre.equals(tarea.nombre) &&
                descripcion.equals(tarea.descripcion) &&
                fechaFin.equals(tarea.fechaFin) &&
                estado == tarea.estado &&
                notificable == tarea.notificable;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
        result = prime * result + ((fechaFin == null) ? 0 : fechaFin.hashCode());
        result = prime * result + ((estado == null) ? 0 : estado.hashCode());
        result = prime * result + ((notificable == null) ? 0 : notificable.hashCode());
        return result;
    }

    @Override
    @NonNull
    public String toString() {
        return "Tarea{" + "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", fechaFin=" + calendarToPrettyString(fechaFin) +
                ", estado=" + estado +
                ", notificable=" + notificable +
                ", categoria=" + (categoria == null ? "null" : categoria.getId()) +
                '}';
    }
}
