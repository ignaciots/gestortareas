package org.ignaciots.gestortareas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.ignaciots.gestortareas.logica.GestorTareas;
import org.ignaciots.gestortareas.model.Tarea;

import java.text.ParseException;

/**
 * Activity que muestra la información completa de una cierta tarea.
 * Muestra su nombre, descripción, fecha, categoría, estado y configuración de notificaciones.
 *
 * @author  Ignacio Torre Suárez
 */
public class VentanaTareaDetallada extends AppCompatActivity {

    private TextView textViewNombre;
    private TextView textViewDescripcion;
    private TextView textViewFecha;
    private TextView textViewCategoria;
    private TextView textViewEstado;
    private TextView textViewNotificar;
    private Button buttonCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_tarea_detallada);
        setTitle(getResources().getText(R.string.detallada_titulo));

        GestorTareas gestor = GestorTareas.getInstance();
        gestor.setManager(this);
        textViewNombre = findViewById(R.id.textViewDetalladaNombreDato);
        textViewDescripcion = findViewById(R.id.textViewDetalladaDescripcionDato);
        textViewFecha = findViewById(R.id.textViewDetalladaFechaFinDato);
        textViewCategoria = findViewById(R.id.textViewDetalladaCategoriaDato);
        textViewEstado = findViewById(R.id.textViewDetalladaEstadoDato);
        textViewNotificar = findViewById(R.id.textViewDetalladaNotificableDato);
        buttonCancelar = findViewById(R.id.buttonTareaDetalladaAtras);

        buttonCancelar.setOnClickListener(new ButtonAtrasOnClickListener());

        Bundle bundle = this.getIntent().getExtras();
        if (bundle == null) {
            throw new IllegalStateException("Bundle no creado correctamente");
        }
        int idTarea = bundle.getInt("org.ignaciots.gestortareas.idtarea");
        try {
            Tarea tarea = gestor.getTarea(idTarea);
            detallarTarea(tarea);
        } catch (ParseException e) {
            toastErrorLeerTareasBD();
            finish();
        }
    }

    /**
     * Muestra los datos de una tarea en los {@link TextView}.
     * @param tarea La tarea cuyos datos serán mostrados.
     */
    private void detallarTarea(Tarea tarea) {
        textViewNombre.setText(tarea.getNombre());
        textViewDescripcion.setText(tarea.getDescripcion());
        textViewFecha.setText(Tarea.calendarToPrettyString(tarea.getFechaFin()));
        textViewCategoria.setText(tarea.getCategoria() == null ? null : tarea.getCategoria().getNombre());
        textViewEstado.setText(tarea.getEstado().getNombre());
        switch (tarea.getEstado()) {
            case COMPLETADO:
                TextViewCompat.setTextAppearance(textViewEstado, R.style.EstadoCompletado);
                break;
            case VENCIDO:
                TextViewCompat.setTextAppearance(textViewEstado, R.style.EstadoVencido);
                break;
        }
        textViewNotificar.setText(tarea.getNotificable().getNombre());
    }

    /**
     * Muestra un toast con información del error producido cuando ocurre un {@link ParseException}
     * al acceder a la base de datos.
     */
    private void toastErrorLeerTareasBD() {
        Toast.makeText(this, getResources().getText(R.string.error_leer_tareas_bd), Toast.LENGTH_LONG).show();
    }

    /**
     * Listener para el botón que sale de la actividad.
     */
    private class ButtonAtrasOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            finish();
        }
    }
}
