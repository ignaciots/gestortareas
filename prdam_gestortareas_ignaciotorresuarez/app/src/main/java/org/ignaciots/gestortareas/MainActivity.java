package org.ignaciots.gestortareas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.TextViewCompat;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import org.ignaciots.gestortareas.logica.GestorTareas;
import org.ignaciots.gestortareas.logica.TipoVista;
import org.ignaciots.gestortareas.logica.filtro.Filtro;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroCualquiera;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNombre;
import org.ignaciots.gestortareas.logica.notificacion.ManagerNotificaciones;
import org.ignaciots.gestortareas.logica.notificacion.NotificacionesBroadcastReceiver;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Tarea;
import org.ignaciots.gestortareas.util.SimpleFileChooser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Actividad principal del Gestor de Tareas. Consta de:
 * Filtro básico: permite filtrar las tareas de forma simple. Disponible el vista tipo lista.
 * Filtro avanzado: se accede pulsando en su botón correspondiente. Disponible el vista tipo lista.
 * Radiobotones de cambio de vista: permiten cambiar entre vista tipo lista y calendario.
 * Lista de tareas: la lista de tareas de aplicación. Cada tarea consta de: nombre, fecha, estado
 * y dos botones: editar y marcar como completada.
 *
 * La actividad principal también posee un menú en la parte superior derecha. Permite crear una
 * nueva tarea e importar y exportar las tareas de la aplicación.
 *
 * @author Ignacio Torre Suárez
 */
public class MainActivity extends AppCompatActivity {

    // Códigos utilizados por la aplicación
    private static final int CODIGO_PERMISOS_ESCRITURA = 0;
    private static final int CODIGO_PERMISOS_LECTURA   = 1;
    private static final int CODIGO_INTENT_ALARMA      = 0;

    // Intervalo de repetición de alarma, en milisegundos
    private static final long INTERVALO_REPETICION_ALARMA = 60000;

    private ViewFlipper viewFlipperListaCalendario;
    private TableLayout tableLayoutTareasLista;
    private TableLayout tableLayeoutTareasCalendario;
    private CalendarView calendarViewTareas;
    private RadioGroup radioGroupVistas;
    private EditText editTextoFiltro;
    private Button buttonFiltroBasico;
    private ImageButton buttonFiltroAvanzado;

    private GestorTareas gestor;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar el gestor de tareas y las vistas
        boolean canalCreado = crearCanalNotificaciones();
        gestor = GestorTareas.getInstance();
        gestor.setManager(this);
        setUpAlertManager();

        viewFlipperListaCalendario = findViewById(R.id.viewFlipperPrincipal);
        tableLayoutTareasLista = findViewById(R.id.tableLayoutTareasLista);
        tableLayeoutTareasCalendario = findViewById(R.id.tableLayoutTareasCalendario);
        calendarViewTareas = findViewById(R.id.calendarViewTareas);
        radioGroupVistas = findViewById(R.id.radioGroupVistas);
        editTextoFiltro = findViewById(R.id.editTextFiltroBasico);
        buttonFiltroBasico = findViewById(R.id.buttonFiltroBasico);
        buttonFiltroAvanzado = findViewById(R.id.imageButtonFiltroAvanzado);

        calendarViewTareas.setOnDayClickListener(new CalendarViewTareasOnDayClickListener());
        radioGroupVistas.setOnCheckedChangeListener(new RadioButtonListaOnCheckedChangeListener());
        buttonFiltroBasico.setOnClickListener(new ButtonFiltroBasicoOnClickListener());
        buttonFiltroAvanzado.setOnClickListener(new ButtonFiltroAvanzadoOnClickListener());

        if (!canalCreado) {
            Toast.makeText(this, getResources().getText(R.string.error_canal_notificaciones), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menuItemNuevaTarea:
                startNuevaTareaActivity();
                break;
            case R.id.menuItemImportarTareas:
                startImportarTareas(this);
                break;
            case R.id.menuItemExportarTareas:
                startExportarTareas(this);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        actualizarVistas();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CODIGO_PERMISOS_ESCRITURA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    exportarTarea(this);
                } else {
                    Toast.makeText(this, getResources().getText(R.string.error_permisos_escritura), Toast.LENGTH_LONG).show();
                }
                break;
            case CODIGO_PERMISOS_LECTURA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    importarTareas(this);
                } else {
                    Toast.makeText(this, getResources().getText(R.string.error_permisos_lectura), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    /**
     * Crea el canal de notificaciones de la aplicación.
     */
    private boolean crearCanalNotificaciones() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence nombreCanal = (ManagerNotificaciones.NOMBRE_CHANNEL_ID);
            String descripcionCanal = ManagerNotificaciones.DESCRIPCION_CHANNEL_ID;
            int importanciaCanal = android.app.NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel canalNotificacion = new NotificationChannel(ManagerNotificaciones.DEFAULT_CHANNEL_ID, nombreCanal, importanciaCanal);
            canalNotificacion.setDescription(descripcionCanal);
            android.app.NotificationManager notificationManager = getSystemService(android.app.NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(canalNotificacion);
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * Inicializa el {@link AlarmManager} con una alarma que llame a {@link NotificacionesBroadcastReceiver} periódicamente.
     */
    private void setUpAlertManager() {
        Intent intent = new Intent(this, NotificacionesBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), CODIGO_INTENT_ALARMA, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + INTERVALO_REPETICION_ALARMA, INTERVALO_REPETICION_ALARMA, pendingIntent);
        } else {
            Toast.makeText(this, getResources().getText(R.string.error_permisos_alarm), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Actualiza las vistas de la actividad
     */
    private void actualizarVistas() {
        // Al acceder a la actividad, modificar las vistas dependiendo de cuál ha sido la última utilizada
        // (Por defecto, vista tipo lista)
        boolean errorObtenerTareas = false;

        if (gestor.getTipoVista() == TipoVista.VISTA_CALENDARIO) {
            radioGroupVistas.check(R.id.radioButtonCalendario);
            setUpVistaCalendario();
        } else {
            // En vista tipo lista solo es necesario añadir las tareas, el resto de componentes están
            // por defecto.
            try {
                if (gestor.isAplicarFiltro()) {
                    addTareasLista(gestor.filtrarTareas());
                } else {
                    addTareasLista(gestor.getTareas());
                }
            } catch (ParseException e) {
                errorObtenerTareas = true;
            }
        }

        if (errorObtenerTareas) {
            toastErrorLeerTareasBD();
        }
    }

    /**
     * Crear una nueva {@link VentanaTareaNueva} para añadir nuevas tareas.
     */
    private void startNuevaTareaActivity() {
        startActivity(new Intent(this, VentanaTareaNueva.class));
    }

    /**
     * Crea una nueva {@link VentanaTareaDetallada} para mostrar una tarea de forma detallada.
     * @param idTarea El id de la tarea a mostrar.
     */
    private void startTareaDetalladaActivity(int idTarea) {
        Intent intent = new Intent(this, VentanaTareaDetallada.class);
        Bundle bundle = new Bundle();
        bundle.putInt("org.ignaciots.gestortareas.idtarea", idTarea);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Crea una nueva {@link VentanaTareaEditar} para editar una tarea concreta.
     * @param idTarea El id de la tarea a editar.
     */
    private void startTareaEditarActivity(int idTarea) {
        Intent intent = new Intent(this, VentanaTareaEditar.class);
        Bundle bundle = new Bundle();
        bundle.putInt("org.ignaciots.gestortareas.idtarea", idTarea);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Crea una nueva {@link VentanaTareaFiltro} para filtrar tareas.
     */
    private void startTareaFiltroActivity() {
        startActivity(new Intent(this, VentanaTareaFiltro.class));
    }

    /**
     * Abre el diálogo de selección de archivo e importa las tareas del archivo seleccionado.
     */
    private void startImportarTareas(Context ctx) {
        // Comprueba si se tienen permisos de lectura para importar archivos. Android 6.0+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!tienePermisosLectura()) {
                pedirPermisosLectura();
            } else {
                importarTareas(ctx);
            }
        } else {
            importarTareas(ctx);
        }

    }

    /**
     * Comprueba si se pueden importar y lanza el diálogo de importar tareas
     * @param ctx El {@link Context} de la aplicación.
     */
    private void importarTareas(Context ctx) {
        // Comprueba si se puede acceder al directorio de archivos de la aplicación
        final File directorioPredeterminado = ctx.getExternalFilesDir(Environment.DIRECTORY_DCIM);
        if (directorioPredeterminado == null) {
            Toast.makeText(this, getResources().getText(R.string.error_noacces_documentos), Toast.LENGTH_LONG).show();
        } else {
            dialogoImportarTareas(directorioPredeterminado);
        }
    }

    /**
     * Lanza el diálogo de importar tareas
     * @param directorioPredeterminado El directorio predeterminado donde se lanzará el diálogo
     */
    private void dialogoImportarTareas(File directorioPredeterminado) {
        SimpleFileChooser fileChooser = new SimpleFileChooser(this, directorioPredeterminado);
        fileChooser.setFileListener(new SimpleFileChooser.FileSelectedListener() {
            @Override
            public void fileSelected(File file) {
                AlertDialog.Builder alterBuilder = new AlertDialog.Builder(MainActivity.this);
                alterBuilder.setTitle(MainActivity.this.getResources().getText(R.string.principal_dialog_importar_tareas
                ));
                alterBuilder.setMessage(String.format(MainActivity.this.getResources().getString(R.string.principal_dialog_importar_descripcion), file.getAbsolutePath()));
                alterBuilder.setCancelable(false);
                alterBuilder.setIcon(R.drawable.download_open_source);
                final File fileImportar = file;
                alterBuilder.setPositiveButton(MainActivity.this.getResources().getString(R.string.principal_dialog_importar_si), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        boolean tareasImportadas = true;
                        try {
                            gestor.importarTareas(fileImportar);
                        } catch (FileNotFoundException e) {
                            Toast.makeText(MainActivity.this, String.format(MainActivity.this.getResources().getString(R.string.error_archivo_no_encontrado), fileImportar.getAbsolutePath()), Toast.LENGTH_SHORT).show();
                            tareasImportadas = false;
                        } catch (IOException e) {
                            Toast.makeText(MainActivity.this, String.format(MainActivity.this.getResources().getString(R.string.error_io), fileImportar.getAbsolutePath()), Toast.LENGTH_SHORT).show();
                            tareasImportadas = false;
                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.error_valido_xml), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            tareasImportadas = false;
                        }

                        if (tareasImportadas) {
                            actualizarVistas();
                            Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.principal_importar_exito), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                alterBuilder.setNegativeButton(MainActivity.this.getResources().getText(R.string.principal_dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });
                alterBuilder.setNeutralButton(MainActivity.this.getResources().getText(R.string.principal_dialog_cancelar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });

                alterBuilder.create().show();

            }
        });
        fileChooser.showDialog();
    }

    /**
     * Pide activar los permisos de lectura de archivos al usuario. Android 6.0+
     */
    private void pedirPermisosLectura() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                CODIGO_PERMISOS_LECTURA);
    }

    /**
     * Comprueba si la aplicación tiene permisos de lectura. Android 6.0+
     * @return true si la aplicación tiene permisos de lectura, false en otro caso.
     */
    private boolean tienePermisosLectura() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Pide activar los permisos de escritura de archivos al usuario. Android 6.0+
     */
    private void pedirPermisosEscritura() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                CODIGO_PERMISOS_ESCRITURA);
    }

    /**
     * Comprueba si la aplicación tiene permisos de escritura. Android 6.0+
     * @return true si la aplicación tiene permisos de escritura, false en otro caso.
     */
    private boolean tienePermisosEscritura() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Pregunta al usuario y exporta las tareas al archivo predeterminado.
     */
    private void startExportarTareas(Context ctx) {
        // Comprueba si se tienen permisos de escritura para exportar el archivo. Android 6.0+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!tienePermisosEscritura()) {
                pedirPermisosEscritura();
            } else {
                exportarTarea(ctx);
            }
        } else {
            exportarTarea(ctx);
        }
    }

    /**
     * Comprueba si se pueden exportar y lanza el diálogo de exportar tarea.
     * @param ctx El {@link Context} de la aplicación.
     */
    private void exportarTarea(Context ctx) {
        final File archivoPredeterminadoExportar = ctx.getExternalFilesDir(Environment.DIRECTORY_DCIM);
        if (archivoPredeterminadoExportar == null) {
            Toast.makeText(ctx, getResources().getText(R.string.error_noacces_documentos), Toast.LENGTH_LONG).show();
        } else {
            dialogoExportarTarea(archivoPredeterminadoExportar);
        }
    }

    /**
     * Lanza el diálogo de exportar tarea
     * @param archivoPredeterminadoExportar el archivo perdeterminado a exportar
     */
    private void dialogoExportarTarea(final File archivoPredeterminadoExportar) {
        final String pathPredeterminadoExportar = archivoPredeterminadoExportar.getAbsolutePath() + "/gestor.xml";
        AlertDialog.Builder alterBuilder = new AlertDialog.Builder(this);
        alterBuilder.setTitle(getResources().getText(R.string.principal_dialog_exportar_title));
        alterBuilder.setMessage(String.format(getResources().getString(R.string.principal_dialog_exportar_message), archivoPredeterminadoExportar));
        alterBuilder.setCancelable(false);
        alterBuilder.setIcon(R.drawable.upload_open_source);

        alterBuilder.setPositiveButton(MainActivity.this.getResources().getString(R.string.principal_dialog_importar_si), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                boolean tareasExportadas = true;
                try {
                    gestor.exportarTareas(pathPredeterminadoExportar);
                } catch (IOException e) {
                    Toast.makeText(MainActivity.this, String.format(MainActivity.this.getResources().getString(R.string.error_io), archivoPredeterminadoExportar), Toast.LENGTH_SHORT).show();
                    tareasExportadas = false;
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.error_generar_xml), Toast.LENGTH_SHORT).show();
                    tareasExportadas = false;
                }
                if (tareasExportadas) {
                    Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.principal_dialog_exportar_exito), Toast.LENGTH_SHORT).show();
                }
            }
        });

        alterBuilder.setNegativeButton(MainActivity.this.getResources().getString(R.string.principal_dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        alterBuilder.setNeutralButton(MainActivity.this.getResources().getString(R.string.principal_dialog_cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        alterBuilder.create().show();
    }

    /**
     * Crea un {@link TableRow} con los datos de una tarea.
     * @param tarea La Tarea a añadir al {@link TableRow}.
     * @return Un {@link TableRow} con los datos básicos de la tarea que podrá ser usada en un {@link TableLayout}.
     */
    private TableRow crearTareaTableRow(Tarea tarea) {
        String nombre = tarea.getNombre();
        String fecha = Tarea.calendarToPrettyString(tarea.getFechaFin());
        String estado = tarea.getEstado().getNombre();

        // Configuración del layout de las vistas de la fila
        TableRow.LayoutParams paramsNombre = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1.6f);
        TableRow.LayoutParams paramsFechaEstado = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1.2f);
        TableRow.LayoutParams paramsBotones = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.5f);
        paramsNombre.setMargins(0, 0, 20, 20);
        paramsFechaEstado.setMargins(0, 0, 6, 20);
        paramsBotones.setMargins(0, 0, 0, 20);
        TableRow filaTarea = new TableRow(this);
        filaTarea.setWeightSum(5);

        // Datos de la tarea
        TextView textViewNombre = new TextView(this);
        textViewNombre.setText(nombre);
        textViewNombre.setLayoutParams(paramsNombre);
        textViewNombre.setId(tarea.getId());
        textViewNombre.setOnClickListener(new TextViewTareaOnClickListener());

        TextView textViewFecha = new TextView(this);
        textViewFecha.setText(fecha);
        textViewFecha.setLayoutParams(paramsFechaEstado);
        textViewFecha.setId(tarea.getId());
        textViewFecha.setOnClickListener(new TextViewTareaOnClickListener());

        TextView textViewEstado = new TextView(this);
        textViewEstado.setText(estado);
        textViewEstado.setLayoutParams(paramsFechaEstado);
        textViewEstado.setId(tarea.getId());
        textViewEstado.setOnClickListener(new TextViewTareaOnClickListener());
        switch (tarea.getEstado()) {
            case COMPLETADO:
                TextViewCompat.setTextAppearance(textViewEstado, R.style.EstadoCompletado);
                break;
            case VENCIDO:
                TextViewCompat.setTextAppearance(textViewEstado, R.style.EstadoVencido);
                break;
        }

        // Botones de la tarea
        final int width = 30;
        final int height = 30;
        ImageButton botonCompletar = new ImageButton(this);
        botonCompletar.setLayoutParams(paramsBotones);
        botonCompletar.setMaxWidth(width);
        botonCompletar.setMaxHeight(height);
        botonCompletar.setOnClickListener(new BotonCompletarOnClickListener());
        botonCompletar.setId(tarea.getId());
        if (tarea.getEstado() == Estado.COMPLETADO) {
            botonCompletar.setBackground(ContextCompat.getDrawable(this, R.drawable.background_transparent));
            botonCompletar.setImageResource(R.drawable.ckeck_disabled);
            botonCompletar.setEnabled(false);
        } else {
            botonCompletar.setImageResource(R.drawable.check_open_source);
        }

        ImageButton botonEditar = new ImageButton(this);
        botonEditar.setImageResource(R.drawable.edit_2_open_source);
        botonEditar.setLayoutParams(paramsBotones);
        botonEditar.setMaxWidth(width);
        botonEditar.setMaxHeight(height);
        botonEditar.setOnClickListener(new BotonEditarOnClickListener());
        botonEditar.setId(tarea.getId());


        // Añadir las vistas a la fila creada
        filaTarea.addView(textViewNombre);
        filaTarea.addView(textViewFecha);
        filaTarea.addView(textViewEstado);
        filaTarea.addView(botonCompletar);
        filaTarea.addView(botonEditar);

        return filaTarea;
    }

    /**
     * Crea una {@link TableRow} con los datos de cabecera (Nombre, Fecha, Estado) de la lista de tareas.
     * @return La {@link TableRow} creada.
     */
    private TableRow crearCabeceraTableRow() {

        // Configuración del layout de las vistas de la fila
        TableRow.LayoutParams paramsNombre = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1.6f);
        paramsNombre.setMargins(0, 0, 0, 76);
        TableRow.LayoutParams paramsFechaEstado = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1.2f);
        paramsNombre.setMargins(0, 0, 0, 76);
        TableRow filaTarea = new TableRow(this);
        filaTarea.setWeightSum(5);

        // Datos de la tarea
        TextView textViewNombre = new TextView(this);
        textViewNombre.setText(getResources().getText(R.string.principal_cabecera_nombre));
        textViewNombre.setLayoutParams(paramsNombre);
        TextViewCompat.setTextAppearance(textViewNombre, R.style.FilaCabecera);

        TextView textViewFecha = new TextView(this);
        textViewFecha.setText(getResources().getText(R.string.principal_cabecera_fecha));
        textViewFecha.setLayoutParams(paramsFechaEstado);
        TextViewCompat.setTextAppearance(textViewFecha, R.style.FilaCabecera);

        TextView textViewEstado = new TextView(this);
        textViewEstado.setText(getResources().getText(R.string.principal_cabecera_estado));
        textViewEstado.setLayoutParams(paramsFechaEstado);
        TextViewCompat.setTextAppearance(textViewEstado, R.style.FilaCabecera);

        filaTarea.addView(textViewNombre);
        filaTarea.addView(textViewFecha);
        filaTarea.addView(textViewEstado);

        return filaTarea;
    }

    /**
     * Actualiza la Activity para mostrar las vistas utilizadas en el modo calendario.
     * Cambia el tipo de vista en el gestor, avanza el {@link ViewFlipper}, hace invisibles
     * los botones de filtrado y añade las tareas al calendario.
     */
    private void setUpVistaCalendario() {
        gestor.setTipoVista(TipoVista.VISTA_CALENDARIO);
        buttonFiltroBasico.setVisibility(View.INVISIBLE);
        buttonFiltroAvanzado.setVisibility(View.INVISIBLE);
        editTextoFiltro.setVisibility(View.INVISIBLE);
        tableLayeoutTareasCalendario.removeAllViews();
        tableLayeoutTareasCalendario.addView(crearCabeceraTableRow());
        try {
            addTareasCalendario();
        } catch (ParseException e) {
            toastErrorLeerTareasBD();
        }
    }

    /**
     * Actualiza la Activity para mostrar las vistas utilizadas en el modo lista.
     * Cambia el tipo de vista en el gestor, modifica el {@link ViewFlipper}, hace visibles
     * los botones de filtrado y añade las tareas a la lista.
     */
    private void setUpVistaLista() {
        gestor.setTipoVista(TipoVista.VISTA_LISTA);
        viewFlipperListaCalendario.showPrevious();
        buttonFiltroBasico.setVisibility(View.VISIBLE);
        buttonFiltroAvanzado.setVisibility(View.VISIBLE);
        editTextoFiltro.setVisibility(View.VISIBLE);
        try {
            addTareasLista(gestor.getTareas());
        } catch (ParseException e) {
            toastErrorLeerTareasBD();
        }
    }

    /**
     * Añade las tareas del gestor al layout de tareas del modo lista.
     */
    private void addTareasLista(List<Tarea> tareas) {
        tableLayoutTareasLista.removeAllViews();
        tableLayoutTareasLista.addView(crearCabeceraTableRow());
        for (Tarea tarea : tareas) {
            tableLayoutTareasLista.addView(crearTareaTableRow(tarea));
        }
    }

    /**
     * Añade las tareas del gestor al calendario en modo calendario.
     */
    private void addTareasCalendario() throws ParseException {
        List<EventDay> eventosCalendario = new ArrayList<>();
        for (Tarea tarea : gestor.getTareas()) {
            eventosCalendario.add(new EventDay(tarea.getFechaFin(), R.drawable.file_text_open_source));
            calendarViewTareas.setEvents(eventosCalendario);
        }
    }

    /**
     * Muestra un toast con información del error producido cuando ocurre un {@link ParseException}
     * al acceder a la base de datos.
     */
     private void toastErrorLeerTareasBD() {
        Toast.makeText(this, getResources().getText(R.string.error_leer_tareas_bd), Toast.LENGTH_LONG).show();
    }

    /**
     * Listener para clicar días del {@link CalendarView}. Muestra las tareas del día seleccionado
     * como una lista.
     */
    private class CalendarViewTareasOnDayClickListener implements OnDayClickListener {

        @Override
        public void onDayClick(EventDay eventDay) {
            Calendar calendarDiaPulsado = eventDay.getCalendar();
            tableLayeoutTareasCalendario.removeAllViews();
            tableLayeoutTareasCalendario.addView(crearCabeceraTableRow());
            try {
                List<Tarea> tareas = gestor.getTareas(calendarDiaPulsado);
                for (Tarea tarea : tareas) {
                    tableLayeoutTareasCalendario.addView(crearTareaTableRow(tarea));
                }
            } catch (ParseException e) {
                toastErrorLeerTareasBD();
            }
        }
    }

    /**
     * Listener para el {@link RadioGroup}. Modifica la vista entre vista tipo lista y calendario.
     */
    private class RadioButtonListaOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.radioButtonCalendario:
                    viewFlipperListaCalendario.showNext();
                    setUpVistaCalendario();
                    break;
                case R.id.radioButtonLista:
                    setUpVistaLista();
                    break;
            }
        }
    }

    /**
     * Listener para el {@link ImageButton} de completar tarea.
     * Pregunta si se está seguro de completar la tarea; en caso afirmativo la completa.
     */
    private class BotonCompletarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final int idTarea = v.getId();

            AlertDialog.Builder alterBuilder = new AlertDialog.Builder(MainActivity.this);
            alterBuilder.setTitle(getResources().getText(R.string.dialog_completar_title));
            alterBuilder.setMessage(getResources().getText(R.string.principal_dialog_completar_message));
            alterBuilder.setCancelable(false);
            alterBuilder.setIcon(R.drawable.check_open_source);

            alterBuilder.setPositiveButton(getResources().getText(R.string.principal_dialog_importar_si), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    try {
                        Tarea tareaCompletar = gestor.getTarea(idTarea);
                        if (tareaCompletar.getEstado() == Estado.COMPLETADO) {
                            Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.principal_dialog_completar_redundante), Toast.LENGTH_SHORT).show();
                        } else {
                            gestor.completarTarea(idTarea);
                            actualizarVistas();
                            Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.principal_dialog_completar_exito), Toast.LENGTH_SHORT).show();
                        }
                    } catch (ParseException e) {
                        toastErrorLeerTareasBD();
                    }
                }
            });

            alterBuilder.setNegativeButton(getResources().getText(R.string.principal_dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alterBuilder.setNeutralButton(R.string.principal_dialog_cancelar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });

            alterBuilder.create().show();
        }
    }

    /**
     * Listener para el {@link ImageButton} de editar tarea.ç
     * Abre la activity de editar tarea.
     */
    private class BotonEditarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int idTarea = v.getId();
            startTareaEditarActivity(idTarea);
        }
    }

    /**
     * Listener para el {@link Button} de ejecutar filtro básico.
     * Ejecuta un filtro básico basado en el nombre de la tarea.
     */
    private class ButtonFiltroBasicoOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String filtro = editTextoFiltro.getText().toString();
            if (!filtro.trim().isEmpty()) {
                List<Filtro> filtros = new ArrayList<>();
                filtros.add(new FiltroNombre(filtro));
                gestor.setFiltros(filtros);
                filtrarTareasActualizar();
            } else { // vacío, mostrar todos los resultados
                List<Filtro> filtros = new ArrayList<>();
                filtros.add(new FiltroCualquiera(new FiltroNombre("")));
                gestor.setFiltros(filtros);
                filtrarTareasActualizar();
            }
        }
    }

    /**
     * Filtra las tareas de la vista de lista y actualiza esta vista.
     */
    private void filtrarTareasActualizar() {
        try {
            List<Tarea> tareasFiltradas = gestor.filtrarTareas();
            addTareasLista(tareasFiltradas);
            Toast.makeText(MainActivity.this, MainActivity.this.getResources().getText(R.string.principal_filtro_correcto), Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
            toastErrorLeerTareasBD();
        }
    }

    /**
     * Listener para el {@link ImageButton} de activity de filtro avanzado.
     * Abre la acivity de filtro avanzado.
     */
    private class ButtonFiltroAvanzadoOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            startTareaFiltroActivity();
        }
    }

    /**
     * Listener para la vista detallada de las tareas.
     * Abre la activity de tarea detallada.
     */
    private class TextViewTareaOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int idTarea = v.getId();
            startTareaDetalladaActivity(idTarea);
        }
    }
}
