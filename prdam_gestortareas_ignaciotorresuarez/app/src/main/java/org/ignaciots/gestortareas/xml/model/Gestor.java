package org.ignaciots.gestortareas.xml.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Clase que representar a la raíz del modelo de datos XML.
 * @author Ignacio Torre Suárez
 */
@Root(name="gestor")
public class Gestor {

    /**
     * Versión actual del gestor XML.
     */
    public static final String VERSION_ACTUAL = "1.0";

    @Attribute
    private String version;

    @ElementList(inline=true, required = false)
    private List<TareaXML> tareas;

    @ElementList(inline=true, required = false)
    private List<CategoriaXML> categorias;

    public Gestor() {}

    public Gestor(String version, List<TareaXML> tareas, List<CategoriaXML> categorias) {
        this.version = version;
        this.tareas = tareas;
        this.categorias = categorias;
    }

    public String getVersion() {
        return version;
    }

    public List<TareaXML> getTareas() {
        return tareas;
    }

    public List<CategoriaXML> getCategorias() {
        return categorias;
    }
}
