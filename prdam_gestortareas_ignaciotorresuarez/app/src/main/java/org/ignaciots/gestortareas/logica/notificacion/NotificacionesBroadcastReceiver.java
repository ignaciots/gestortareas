package org.ignaciots.gestortareas.logica.notificacion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * {@link BroadcastReceiver} que se encarga de iniciar el {@link ServicioNotificaciones}
 * cuando este es llamado por el {@link android.app.AlarmManager}.
 * @author Ignacio Torre Suárez
 */
public class NotificacionesBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentService = new Intent(context, ServicioNotificaciones.class);
        context.startService(intentService);
    }
}
