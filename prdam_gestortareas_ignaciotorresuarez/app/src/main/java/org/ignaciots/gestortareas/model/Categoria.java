package org.ignaciots.gestortareas.model;

import androidx.annotation.NonNull;

/**
 * Clase que representa a la entidad Categoría utilizada en el modelo de la base de datos.
 * @author Ignacio Torre Suárez
 */
public class Categoria {

    private int id;
    private String nombre;


    public Categoria() {}

    public Categoria(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return id == categoria.id &&
                nombre.equals(categoria.nombre);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }

    @Override
    @NonNull
    public String toString() {
        return "Categoria{" + "id=" + id +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
