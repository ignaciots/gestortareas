package org.ignaciots.gestortareas.logica.filtro;

import org.ignaciots.gestortareas.model.Tarea;

/**
 * Clase abstract que implementa funcionalidad básica común de los filtros de tipo texto.
 * Los filtros de tipo texto no filtran por texto exacto, sino que comprueban que el texto a filtrar
 * esté contenido en el texto.
 * @author Ignacio Torre Suárez
 */
public abstract class AbstractFiltroTexto extends AbstractFiltro {

    protected String texto;

    protected AbstractFiltroTexto(String texto) {
        super();
        if (texto == null) {
            throw new IllegalArgumentException("El texto no puede ser null");
        }
        this.texto = texto;
    }

    @Override
    protected int comparar(Tarea tarea) {
        if (getTextoTarea(tarea) == null) {
            return -1;
        }
        return getTextoTarea(tarea).contains(texto) ? 0 : 1;
    }

    protected abstract String getTextoTarea(Tarea tarea);
}
