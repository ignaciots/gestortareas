package org.ignaciots.gestortareas.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import org.ignaciots.gestortareas.model.Categoria;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;

import java.util.Calendar;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


/**
 * Clase que se encarga de realizar consultas SQL e interactura con la base de datos SQLite de
 * la aplicación.
 *
 * @author Ignacio Torre Suárez
 */
public class SQLiteManager extends SQLiteOpenHelper {

    /**
     * Nombre identificador utilizado para la base de datos
     */
    public static final String NOMBRE_MANAGER_SQLITE = "org.ignaciots.dbtareas";

    // Consultas SQL
    private static final String SQL_CREATE_TABLE_CATEGORIA = "CREATE TABLE IF NOT EXISTS CATEGORIA " +
                                    "(id_categoria INTEGER CONSTRAINT PK_CATEGORIA PRIMARY KEY," +
                                    "nombre TEXT UNIQUE NOT NULL);";
    private static final String SQL_CREATE_TABLE_TAREA = "CREATE TABLE IF NOT EXISTS TAREA " +
            "(id_tarea INTEGER CONSTRAINT PK_TAREA PRIMARY KEY," +
            "nombre TEXT NOT NULL," +
            "descripcion TEXT," +
            "fecha TEXT NOT NULL," +
            "estado INTEGER NOT NULL," +
            "notificable INTEGER NOT NULL," +
            "id_categoria INTEGER," +
            "CONSTRAINT CK_TAREA_ESTADO CHECK (estado BETWEEN 0 AND 2)," +
            "CONSTRAINT CK_TAREA_NOTIFICABLE CHECK (notificable BETWEEN 0 AND 1)," +
            "CONSTRAINT FK_TAREA_CATEGORIA FOREIGN KEY (id_categoria) REFERENCES CATEGORIA (id_categoria) ON DELETE SET NULL);";

    private static final String SQL_SELECT_TAREAS                     = "SELECT id_tarea, tarea.nombre, descripcion, fecha, estado, notificable, categoria.id_categoria, CATEGORIA.nombre FROM TAREA LEFT JOIN CATEGORIA ON tarea.id_categoria=CATEGORIA.id_categoria ORDER BY datetime(fecha) ASC;";
    private static final String SQL_SELECT_TAREA_BY_ID                = "SELECT id_tarea, tarea.nombre, descripcion, fecha, estado, notificable, categoria.id_categoria, CATEGORIA.nombre FROM TAREA LEFT JOIN CATEGORIA ON tarea.id_categoria=CATEGORIA.id_categoria WHERE tarea.id_tarea=?;";
    private static final String SQL_SELECT_TAREAS_BY_FECHA_AND_ESTADO = "SELECT id_tarea, tarea.nombre, descripcion, fecha, estado, notificable, categoria.id_categoria, CATEGORIA.nombre FROM TAREA LEFT JOIN CATEGORIA ON tarea.id_categoria=CATEGORIA.id_categoria WHERE tarea.fecha <=? AND estado=? AND notificable=1;";
    private static final String SQL_SELECT_TAREAS_LIKE_FECHA          = "SELECT id_tarea, tarea.nombre, descripcion, fecha, estado, notificable, categoria.id_categoria, CATEGORIA.nombre FROM TAREA LEFT JOIN CATEGORIA ON tarea.id_categoria=CATEGORIA.id_categoria WHERE tarea.fecha LIKE ?;";
    private static final String SQL_SELECT_CATEGORIAS                 = "SELECT id_categoria, nombre FROM CATEGORIA;";
    private static final String SQL_SELECT_CATEGORIA_BY_ID            = "SELECT id_categoria, nombre FROM CATEGORIA WHERE id_categoria=?;";
    private static final String SQL_SELECT_CATEGORIA_BY_NAME          = "SELECT id_categoria, nombre FROM CATEGORIA WHERE nombre=?;";

    // Nombres de las tablas de la BD
    private static final String SQL_NOMBRE_TABLA_TAREA     = "TAREA";
    private static final String SQL_NOMBRE_TABLA_CATEGORIA = "CATEGORIA";

    // Índices de columna utilizados en los cursores
    private static final int SQL_INDICE_COLUMNA_ID_TAREA               = 0;
    private static final int SQL_INDICE_COLUMNA_NOMBRE_TAREA           = 1;
    private static final int SQL_INDICE_COLUMNA_DESCRIPCION_TAREA      = 2;
    private static final int SQL_INDICE_COLUMNA_FECHA_TAREA            = 3;
    private static final int SQL_INDICE_COLUMNA_ESTADO_TAREA           = 4;
    private static final int SQL_INDICE_COLUMNA_NOTIFICABLE_TAREA      = 5;
    private static final int SQL_INDICE_COLUMNA_ID_CATEGORIA_TAREA     = 6;
    private static final int SQL_INDICE_COLUMNA_NOMBRE_CATEGORIA_TAREA = 7;

    private static final int SQL_INDICE_COLUMNA_ID_CATEGORIA           = 0;
    private static final int SQL_INDICE_COLUMNA_NOMBRE_CATEGORIA       = 1;

    // Nombres de columna utilizados a la hora de insertar o actualizar datos
    private static final String SQL_NOMBRE_COLUMNA_NOMBRE_TAREA       = "nombre";
    private static final String SQL_NOMBRE_COLUMNA_DESCRIPCION_TAREA  = "descripcion";
    private static final String SQL_NOMBRE_COLUMNA_FECHA_TAREA        = "fecha";
    private static final String SQL_NOMBRE_COLUMNA_ESTADO_TAREA       = "estado";
    private static final String SQL_NOMBRE_COLUMNA_NOTIFICABLE_TAREA  = "notificable";
    private static final String SQL_NOMBRE_COLUMNA_ID_CATEGORIA_TAREA = "id_categoria";

    private static final String SQL_NOMBRE_COLUMNA_NOMBRE_CATEGORIA   = "nombre";


    /**
     * Creea un nuevo {@link SQLiteManager}.
     * @param context El {@link Context} de la aplicación.
     * @param name El nombre identificador de la base de datos.
     * @param factory {@link android.database.sqlite.SQLiteDatabase.CursorFactory} de {@link SQLiteDatabase}.
     * @param version Versión de {@link SQLiteDatabase}.
     */
    public SQLiteManager(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_CATEGORIA);
        db.execSQL(SQL_CREATE_TABLE_TAREA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    /**
     * Obtiene una lista de todas las tareas de la base de datos.
     * @return Una {@link List} de {@link Tarea} obtenidas. Puede estar vacía.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public List<Tarea> getTareas() throws ParseException {
        SQLiteDatabase db = getReadableDatabase();
        List<Tarea> tareas = new ArrayList<>();
        Cursor cursor = db.rawQuery(SQL_SELECT_TAREAS, null);
        while (cursor.moveToNext()) {
            Tarea tarea = getTareaFromCursor(cursor);
            tareas.add(tarea);
        }

        cursor.close();
        db.close();

        return tareas;
    }

    /**
     * Obtiene una tarea de la base de datos basado en su id.
     * @param idTarea El id de la tarea a obtener.
     * @return La {@link Tarea} con el id obtenido, o null si no existe.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public Tarea getTareaById(int idTarea) throws ParseException {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_SELECT_TAREA_BY_ID, new String[]{Integer.toString(idTarea)});
        Tarea tarea = null;
        if (cursor.moveToNext()) {
            tarea = getTareaFromCursor(cursor);
        }

        cursor.close();
        db.close();

        return tarea;
    }

    /**
     * Obtiene una lista de todas las tareas con fecha menor o igual que la establecida y el estado
     * establecido, que además tengan el estado notificable.
     * @param fechaWhere La fecha usada para obtener las tareas.
     * @param estadoWhere El estado usado para obtener las tareas.
     * @return Una {@link List} de {@link Tarea} obtenidas. Puede estar vacía.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public List<Tarea> getTareasByFechaAndEstado(Calendar fechaWhere, Estado estadoWhere) throws ParseException {
        SQLiteDatabase db = getReadableDatabase();
        List<Tarea> tareas = new ArrayList<>();
        Cursor cursor = db.rawQuery(SQL_SELECT_TAREAS_BY_FECHA_AND_ESTADO, new String[]{Tarea.calendarToString(fechaWhere), Integer.toString(estadoWhere.getId())});
        while (cursor.moveToNext()) {
            Tarea tarea = getTareaFromCursor(cursor);
            tareas.add(tarea);
        }

        cursor.close();
        db.close();

        return tareas;
    }

    /**
     * Obtiene una lista de todas las tareas con fecha similar a la establecida.
     * @param fechaLike La fecha utilizada para obtener las tareas.
     * @return Una {@link List} de {@link Tarea} obtenidas. Puede estar vacía.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public List<Tarea> getTareasLikeFecha(String fechaLike) throws ParseException {
        SQLiteDatabase db = getReadableDatabase();
        List<Tarea> tareas = new ArrayList<>();
        Cursor cursor = db.rawQuery(SQL_SELECT_TAREAS_LIKE_FECHA, new String[]{fechaLike + "%"});
        while (cursor.moveToNext()) {
            Tarea tarea = getTareaFromCursor(cursor);
            tareas.add(tarea);
        }

        cursor.close();
        db.close();

        return tareas;
    }

    /**
     * Elimina la tarea con el id establecido.
     * @param idTarea El id utilizado para borrar la tarea.
     * @return El número de filas borradas al ejecutar la consulta.
     */
    public int removeTarea(int idTarea) {
        SQLiteDatabase db = getWritableDatabase();
        int filasBorradas = db.delete(SQL_NOMBRE_TABLA_TAREA, "id_tarea=?;", new String[]{Integer.toString(idTarea)});
        db.close();

        return filasBorradas;
    }

    /**
     * Actualiza los datos de una tarea por su id.
     * @param idTarea El id de la tarea a actualizar.
     * @param nuevaTarea Objeto {@link Tarea} con los nuevos datos de la tarea.
     * @return El número de filas actualizadas al ejecutar la consulta.
     */
    public int updateTarea(int idTarea, Tarea nuevaTarea) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues registros = new ContentValues();
        registros.put(SQL_NOMBRE_COLUMNA_NOMBRE_TAREA, nuevaTarea.getNombre());
        registros.put(SQL_NOMBRE_COLUMNA_DESCRIPCION_TAREA, nuevaTarea.getDescripcion());
        registros.put(SQL_NOMBRE_COLUMNA_FECHA_TAREA, Tarea.calendarToString(nuevaTarea.getFechaFin()));
        registros.put(SQL_NOMBRE_COLUMNA_ESTADO_TAREA, nuevaTarea.getEstado().getId());
        registros.put(SQL_NOMBRE_COLUMNA_NOTIFICABLE_TAREA, nuevaTarea.getNotificable().getId());
        registros.put(SQL_NOMBRE_COLUMNA_ID_CATEGORIA_TAREA, nuevaTarea.getCategoria() == null ? null : nuevaTarea.getCategoria().getId());

        int filasActualizadas = db.update(SQL_NOMBRE_TABLA_TAREA, registros, "id_tarea=?;", new String[]{Integer.toString(idTarea)});
        db.close();

        return filasActualizadas;
    }

    /**
     * Actualiza el estado de una tarea por su id.
     * @param idTarea El id de la tarea a actualizar.
     * @param estado El nuevo {@link Estado} de la tarea.
     * @return El número de filas actualizadas al ejecutar la consulta.
     */
    public int updateTareaEstado(int idTarea, Estado estado) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues registros = new ContentValues();
        registros.put(SQL_NOMBRE_COLUMNA_ESTADO_TAREA, estado.getId());
        int filasActualizadas = db.update(SQL_NOMBRE_TABLA_TAREA, registros, "id_tarea=?;", new String[]{Integer.toString(idTarea)});

        db.close();

        return filasActualizadas;
    }

    /**
     * Actualiza el estado de una tarea cuya fecha menor o igual que la establecida y el estado sea sin completar
     * a estado vencida.
     * @param fechaAhora La fecha usada a la hora de ejecutar la consulta.
     * @return El número de filas actualizadas al ejecutar la consulta.
     */
    public int updateTareasEstado(Calendar fechaAhora) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues registros = new ContentValues();
        registros.put(SQL_NOMBRE_COLUMNA_ESTADO_TAREA, Estado.VENCIDO.getId());
        int filasActualizadas = db.update(SQL_NOMBRE_TABLA_TAREA, registros, "fecha<=? AND estado=?;",
                new String[]{Tarea.calendarToString(fechaAhora), Integer.toString(Estado.SIN_COMPLETAR.getId())});

        db.close();

        return filasActualizadas;
    }

    /**
     * Inserta una tarea en la base de datos.
     * @param tarea La tarea a insertar.
     * @return El número de registros insertados correctamente.
     */
    public long createTarea(Tarea tarea) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues registros = new ContentValues();
        registros.put(SQL_NOMBRE_COLUMNA_NOMBRE_TAREA, tarea.getNombre());
        registros.put(SQL_NOMBRE_COLUMNA_DESCRIPCION_TAREA, tarea.getDescripcion());
        registros.put(SQL_NOMBRE_COLUMNA_FECHA_TAREA, Tarea.calendarToString(tarea.getFechaFin()));
        registros.put(SQL_NOMBRE_COLUMNA_ESTADO_TAREA, tarea.getEstado().getId());
        registros.put(SQL_NOMBRE_COLUMNA_NOTIFICABLE_TAREA, tarea.getNotificable().getId());
        registros.put(SQL_NOMBRE_COLUMNA_ID_CATEGORIA_TAREA, tarea.getCategoria() == null ? null : tarea.getCategoria().getId());

        long filasInsertadas = db.insert(SQL_NOMBRE_TABLA_TAREA, null, registros);

        db.close();

        return filasInsertadas;
    }

    /**
     * Obtiene una lista de las categorías de la base de datos.
     * @return Una {@link List} de {@link Categoria} obtenidas. Puede estar vacía.
     */
    public List<Categoria> getCategorias() {
        SQLiteDatabase db = getReadableDatabase();
        List<Categoria> categorias = new ArrayList<>();
        Cursor cursor = db.rawQuery(SQL_SELECT_CATEGORIAS, null);
        while (cursor.moveToNext()) {
            Categoria categoria = getCategoriaFromCursor(cursor);
            categorias.add(categoria);
        }

        cursor.close();
        db.close();

        return categorias;
    }

    /**
     * Obtiene la categoría con el id pedido.
     * @param idCategoria EL id de la categoría a obtener.
     * @return La {@link Categoria} obtenida.
     */
    public Categoria getCategoriaById(int idCategoria) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_SELECT_CATEGORIA_BY_ID, new String[]{Integer.toString(idCategoria)});
        Categoria categoria= null;
        if (cursor.moveToNext()) {
            categoria = getCategoriaFromCursor(cursor);
        }

        cursor.close();
        db.close();

        return categoria;
    }

    /**
     * Obtiene la categoría con el nombre pedido.
     * @param nombre EL nombre de la categoría a obtener.
     * @return La {@link Categoria} obtenida.
     */
    public Categoria getCategoriaByName(String nombre) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_SELECT_CATEGORIA_BY_NAME, new String[]{nombre});
        Categoria categoria= null;
        if (cursor.moveToNext()) {
            categoria =getCategoriaFromCursor(cursor);
        }

        cursor.close();
        db.close();

        return categoria;
    }

    /**
     * Inserta una categoría en la base de datos.
     * @param categoria La nueva categoría a insertar.
     * @return El número de registros insertados correctamente.
     */
    public long createCategoria(Categoria categoria) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues registros = new ContentValues();
        registros.put(SQL_NOMBRE_COLUMNA_NOMBRE_CATEGORIA, categoria.getNombre());

        long filasInsertadas  = db.insert(SQL_NOMBRE_TABLA_CATEGORIA, null, registros);

        db.close();

        return filasInsertadas;
    }

    /**
     * Crea una tarea obteniendo sus datos de registro del curso dado.
     * @param cursor El {@link Cursor} utilizado.
     * @return La {@link Tarea} creada a partir del cursor.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    private Tarea getTareaFromCursor(Cursor cursor) throws ParseException {
        int id = cursor.getInt(SQL_INDICE_COLUMNA_ID_TAREA);
        String nombre = cursor.getString(SQL_INDICE_COLUMNA_NOMBRE_TAREA);
        String descripcion = cursor.getString(SQL_INDICE_COLUMNA_DESCRIPCION_TAREA);
        Calendar fecha = Tarea.stringToCalendar(cursor.getString(SQL_INDICE_COLUMNA_FECHA_TAREA));
        Estado estado = Tarea.integerToEstado(cursor.getInt(SQL_INDICE_COLUMNA_ESTADO_TAREA));
        Notificable notificable = Tarea.integerToNotificable(cursor.getInt(SQL_INDICE_COLUMNA_NOTIFICABLE_TAREA));
        int idCategoria = cursor.getInt(SQL_INDICE_COLUMNA_ID_CATEGORIA_TAREA);
        String nombreCategoria = cursor.getString(SQL_INDICE_COLUMNA_NOMBRE_CATEGORIA_TAREA);
        return new Tarea(id, nombre, descripcion, fecha, estado, notificable, new Categoria(idCategoria, nombreCategoria));
    }

    /**
     * Crea una categoría obteniendo sus datos de registro del curso dado.
     * @param cursor El {@link Cursor} utilizado.
     * @return La {@link Categoria} creada a partir del cursor.
     */
    private Categoria getCategoriaFromCursor(Cursor cursor) {
        int id = cursor.getInt(SQL_INDICE_COLUMNA_ID_CATEGORIA);
        String nombre = cursor.getString(SQL_INDICE_COLUMNA_NOMBRE_CATEGORIA);
        return new Categoria(id, nombre);
    }


}
