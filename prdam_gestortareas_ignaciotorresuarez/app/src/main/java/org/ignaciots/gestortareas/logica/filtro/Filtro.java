package org.ignaciots.gestortareas.logica.filtro;

import org.ignaciots.gestortareas.model.Tarea;

import java.util.List;

/**
 * Interfaz de filtro utilizado para filtra tareas.
 * @author Ignacio Torre Suárez
 */
public interface Filtro {

    /**
     * Filtra una lista de tareas.
     * @param tareas La {@link List} de {@link Tarea} a filtrar.
     * @return La {@link List} de {@link Tarea} filtrada.
     */
    List<Tarea> filtrarTareas(List<Tarea> tareas);

    /**
     * Obtiene un array con el estado actual del filtro de tareas.
     * El estado del filtro depende del tipo de filtro. Por lo general, este array tendrá una entrada
     * por cada tarea cuyo valor puede ser 1, 0 o -1. Este valor servirá para filtrar las tareas por
     * filtros que hereden {@link AbstractFiltroDecorator}.
     * @return Un array de int con el estado de los filtros.
     */
    int[] getEstadoFiltro();
}
