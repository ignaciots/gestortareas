package org.ignaciots.gestortareas;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.ignaciots.gestortareas.logica.GestorTareas;
import org.ignaciots.gestortareas.model.Categoria;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Activity que muestra la información completa de una cierta tarea y permite editarla.
 * Muestra su nombre, descripción, fecha, categoría, estado y configuración de notificaciones.
 *
 * @author  Ignacio Torre Suárez
 */
public class VentanaTareaEditar extends AppCompatActivity {

    private EditText editTextNombre;
    private EditText editTextDescripcion;
    private Button btnFechaCambiar;
    private Button btnHoraCambiar;
    private TextView textViewFecha;
    private TextView textViewHora;
    private EditText editTextCategoria;
    private Spinner spinnerEstado;
    private CheckBox checkBoxNotificar;
    private Button buttonGuardar;
    private Button buttonEliminar;
    private Button buttonCancelar;

    private GestorTareas gestor;
    private Tarea tareaEditar;
    private Integer anyoSeleccionado = null;
    private Integer mesSeleccionado = null;
    private Integer diaSeleccionado = null;
    private Integer horaSeleccionada = null;
    private Integer minutoSeleccionado = null;
    private Estado estadoSeleccionado = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_tarea_editar);
        setTitle(getResources().getText(R.string.editar_titulo));

        gestor = GestorTareas.getInstance();
        gestor.setManager(this);
        editTextNombre = findViewById(R.id.editTextEditarNombreDato);
        editTextDescripcion = findViewById(R.id.editTextEditarDescripcionDato);
        btnFechaCambiar = findViewById(R.id.buttonEditarFechaCambiar);
        btnHoraCambiar = findViewById(R.id.buttonEditarHoraCambiar);
        textViewFecha = findViewById(R.id.textViewEditarFechaFinDato);
        textViewHora = findViewById(R.id.textViewEditarHoraDato);
        editTextCategoria = findViewById(R.id.editTextEditarCategoriaDato);
        spinnerEstado = findViewById(R.id.spinnerEditarEstadoDato);
        checkBoxNotificar = findViewById(R.id.checkBoxEditarNotificarDato);
        buttonGuardar = findViewById(R.id.buttonEditarCrear);
        buttonEliminar = findViewById(R.id.buttonEditarEliminar);
        buttonCancelar = findViewById(R.id.buttonEditarCancelar);

        btnFechaCambiar.setOnClickListener(new ButtonFechaCambiarOnClickListener());
        btnHoraCambiar.setOnClickListener(new ButtonHoraCambiarOnClickListener());
        spinnerEstado.setOnItemSelectedListener(new SpinnerEstadoOnItemSelectedListener());
        buttonGuardar.setOnClickListener(new ButtonGuardarOnClickListener());
        buttonEliminar.setOnClickListener(new ButtonEliminarOnClickListener());
        buttonCancelar.setOnClickListener(new ButtonCancelarOnClickListener());

        Bundle bundle = this.getIntent().getExtras();
        if (bundle == null) {
            throw new IllegalStateException("Bundle no creado correctamente");
        }
        int idTarea = bundle.getInt("org.ignaciots.gestortareas.idtarea");
        try {
            tareaEditar = gestor.getTarea(idTarea);
            setUpDatosTarea();
        } catch (ParseException e) {
            toastErrorLeerTareasBD();
            finish();
        }
    }

    /**
     * Inicializa los datos de la tarea a editar.
     */
    private void setUpDatosTarea() {
        editTextNombre.setText(tareaEditar.getNombre());
        editTextDescripcion.setText(tareaEditar.getDescripcion());
        editTextCategoria.setText(tareaEditar.getCategoria() == null ? null : tareaEditar.getCategoria().getNombre());
        if (tareaEditar.getNotificable() == Notificable.NOTIFICABLE) {
            checkBoxNotificar.setChecked(true);
        } else {
            checkBoxNotificar.setChecked(false);
        }

        Calendar fechaEditar = tareaEditar.getFechaFin();
        anyoSeleccionado = fechaEditar.get(Calendar.YEAR);
        mesSeleccionado = fechaEditar.get(Calendar.MONTH);
        diaSeleccionado = fechaEditar.get(Calendar.DAY_OF_MONTH);
        horaSeleccionada = fechaEditar.get(Calendar.HOUR_OF_DAY);
        minutoSeleccionado = fechaEditar.get(Calendar.MINUTE);
        textViewFecha.setText(Tarea.calendarDateToPrettyString(fechaEditar));
        textViewHora.setText(Tarea.calendarTimeToPrettyString(fechaEditar));

        setUpSpinnerEstado();
    }

    /**
     * Inicializa el spinner que muestra todos los estados posibles de una tarea.
     */
    private void setUpSpinnerEstado() {
        List<Estado> estados = new ArrayList<>(Arrays.asList(Estado.values()));

        ArrayAdapter<Estado> adapterSpinnerEstado = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, estados);
        adapterSpinnerEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstado.setAdapter(adapterSpinnerEstado);
        spinnerEstado.setSelection(tareaEditar.getEstado().getId());
    }

    /**
     * Muestra un toast con información del error producido cuando ocurre un {@link ParseException}
     * al acceder a la base de datos.
     */
    private void toastErrorLeerTareasBD() {
        Toast.makeText(this, getResources().getText(R.string.error_leer_tareas_bd), Toast.LENGTH_LONG).show();
    }

    /**
     * Listener para el botón de fecha que lanza el diálogo de selección de fecha {@link DatePickerDialog}.
     */
    private class ButtonFechaCambiarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // Lanzar el diálogo con la fecha actual seleccionada
            DatePickerDialog dialogoFecha = new DatePickerDialog(VentanaTareaEditar.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Actualizar la fecha seleccionada para crear la nueva tarea
                            VentanaTareaEditar.this.anyoSeleccionado = year;
                            VentanaTareaEditar.this.mesSeleccionado = monthOfYear;
                            VentanaTareaEditar.this.diaSeleccionado = dayOfMonth;
                            Calendar fechaSeleccionada = Calendar.getInstance();
                            fechaSeleccionada.set(anyoSeleccionado, mesSeleccionado, diaSeleccionado);
                            textViewFecha.setText(Tarea.calendarDateToPrettyString(fechaSeleccionada));
                        }
                    }, anyoSeleccionado, mesSeleccionado, diaSeleccionado);
            dialogoFecha.show();
        }
    }

    /**
     * Listener para el botón de fecha que lanza el diálogo de selección de fecha {@link TimePickerDialog}.
     */
    private class ButtonHoraCambiarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // Lanzar el diálogo con la hora actual seleccionada
            TimePickerDialog dialogoHora = new TimePickerDialog(VentanaTareaEditar.this,
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            // Actualizar la hora seleccionada para crear la nueva tarea
                            VentanaTareaEditar.this.horaSeleccionada = hourOfDay;
                            VentanaTareaEditar.this.minutoSeleccionado = minute;
                            Calendar hora = Calendar.getInstance();
                            hora.set(Calendar.HOUR_OF_DAY, horaSeleccionada);
                            hora.set(Calendar.MINUTE, minutoSeleccionado);
                            textViewHora.setText(Tarea.calendarTimeToPrettyString(hora));
                        }
                    }, horaSeleccionada, minutoSeleccionado, true);
            dialogoHora.show();
        }
    }

    /**
     * Listener utilizada para establecer el estado de la nueva tarea a crear.
     */
    private class SpinnerEstadoOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            estadoSeleccionado = (Estado) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    /**
     * Listener para el botón que guarda los cambios de la tarea en edición.
     */
    private class ButtonGuardarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String nombre = editTextNombre.getText().toString();
            String descripcion = editTextDescripcion.getText().toString();
            String categoria = editTextCategoria.getText().toString();

            // Comprobar campos obligatorios
            if (nombre.trim().isEmpty() || anyoSeleccionado == null
                    || horaSeleccionada == null || estadoSeleccionado == null) {
                Toast.makeText(VentanaTareaEditar.this, VentanaTareaEditar.this.getResources().getText(R.string.tarea_campos_obligatorios), Toast.LENGTH_LONG).show();
            } else {
                if (!categoria.trim().isEmpty()) {
                    // Comprobar si la categoría ya existe
                    Categoria categoriaExistente = gestor.getCategoria(categoria);
                    if (categoriaExistente == null) {
                        categoriaExistente = new Categoria();
                        categoriaExistente.setNombre(categoria);
                        gestor.crearCategoria(categoriaExistente);
                        tareaEditar.setCategoria(gestor.getCategoria(categoria));
                    } else {
                        tareaEditar.setCategoria(categoriaExistente);
                    }
                } else {
                    tareaEditar.setCategoria(null);
                }

                tareaEditar.setNombre(nombre);
                tareaEditar.setDescripcion(descripcion.trim().isEmpty() ? null : descripcion);
                Calendar calendario = Calendar.getInstance();
                calendario.set(anyoSeleccionado, mesSeleccionado, diaSeleccionado, horaSeleccionada, minutoSeleccionado);
                tareaEditar.setFechaFin(calendario);
                tareaEditar.setEstado(estadoSeleccionado);
                if (checkBoxNotificar.isChecked()) {
                    tareaEditar.setNotificable(Notificable.NOTIFICABLE);
                } else {
                    tareaEditar.setNotificable(Notificable.NO_NOTIFICABLE);
                }

                gestor.editarTarea(tareaEditar.getId(), tareaEditar);

                finish();
            }
        }
    }

    /**
     * Listener para el botón que elimina la tarea.
     * Pregunta si la tarea quiere eliminarse y en caso afirmativo, la elimina,
     */
    private class ButtonEliminarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            AlertDialog.Builder alterBuilder = new AlertDialog.Builder(VentanaTareaEditar.this);
            alterBuilder.setTitle(getResources().getText(R.string.editar_dialog_borrar_title));
            alterBuilder.setMessage(getResources().getText(R.string.editar_dialog_borrar_message));
            alterBuilder.setCancelable(false);
            alterBuilder.setIcon(R.drawable.trash_2_open_source);

            alterBuilder.setPositiveButton(VentanaTareaEditar.this.getResources().getText(R.string.principal_dialog_importar_si), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    gestor.borrarTarea(tareaEditar.getId());
                    finish();
                    Toast.makeText(VentanaTareaEditar.this, VentanaTareaEditar.this.getResources().getText(R.string.editar_dialog_borrar_exito), Toast.LENGTH_SHORT).show();
                }
            });

            alterBuilder.setNegativeButton(getResources().getText(R.string.principal_dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alterBuilder.setNeutralButton(getResources().getText(R.string.principal_dialog_cancelar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });

            alterBuilder.create().show();
        }
    }

    /**
     * Listener para el botón que sale de la tarea.
     */
    private class ButtonCancelarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            finish();
        }
    }
}
