package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroDecorator;
import org.ignaciots.gestortareas.logica.filtro.Filtro;

/**
 * Filtro decorator utilizado para filtrar tareas cuyo resultado del filtro haya sido cualquiera.
 * @author Ignacio Torre Suárez
 */
public class FiltroCualquiera extends AbstractFiltroDecorator {

    /**
     * Crea un filtro que filtra por cualquier resultado del filtro pasado como parámetro.
     * @param filtro El filtro a ejecutar.
     */
    public FiltroCualquiera(Filtro filtro) {
        super(filtro);
    }

    @Override
    protected int doFiltrarEstado(int estado) {
        return 0;
    }
}
