package org.ignaciots.gestortareas.logica.filtro.impl;


import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroTexto;
import org.ignaciots.gestortareas.model.Tarea;


/**
 * Filtro utilizado para filtrar categorías.
 * @author Ignacio Torre Suárez
 */
public class FiltroCategoria extends AbstractFiltroTexto {

    /**
     * Crea un filtro que filtra por categorías que contienen el texto dado.
     * @param texto El texto usado en el filtro.
     */
    public FiltroCategoria(String texto) {
        super(texto);
    }

    @Override
    protected String getTextoTarea(Tarea tarea) {
        return tarea.getCategoria() == null ? null : tarea.getCategoria().getNombre();
    }
}
