package org.ignaciots.gestortareas.logica;

import android.content.Context;

import org.ignaciots.gestortareas.R;
import org.ignaciots.gestortareas.db.SQLiteManager;
import org.ignaciots.gestortareas.logica.filtro.Filtro;
import org.ignaciots.gestortareas.logica.notificacion.ManagerNotificaciones;
import org.ignaciots.gestortareas.model.Categoria;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Tarea;
import org.ignaciots.gestortareas.xml.XMLManager;
import org.ignaciots.gestortareas.xml.model.CategoriaXML;
import org.ignaciots.gestortareas.xml.model.Gestor;
import org.ignaciots.gestortareas.xml.model.TareaXML;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Clase que se encarga de las operaciones lógicas del gestor de tareas: obtener tareas, crearlas,
 * enviar notifiaciones, importar, exportar, etc.
 * @author Ignacio Torre Suárez
 */
public class GestorTareas {

    // Ubicación predeterminada del archivo XML de tareas exportadas

    private static GestorTareas instanciaGestor = null;

    // Tipo de la última vista accedida (lista, calendario)
    private TipoVista ultimaVistaAccedida;

    // Indica si existen filtros que deben ser aplicados a las vistas
    private boolean aplicarFiltro;
    private List<Filtro> filtros;

    private final XMLManager xmlManager;
    private final ManagerNotificaciones notificationManager;
    private SQLiteManager sqlManager = null;


    private GestorTareas() {
        this.xmlManager = new XMLManager();
        this.notificationManager = new ManagerNotificaciones();
        this.filtros = null;
        ultimaVistaAccedida = TipoVista.VISTA_LISTA;
        aplicarFiltro = false;
    }

    /**
     * Obtiene la única instancia accesible del {@link GestorTareas}.
     * Si esta es accedida por primera vez será inicializada.
     * @return La instacia del {@link GestorTareas}.
     */
    public static GestorTareas getInstance() {
        if (instanciaGestor == null) {
            instanciaGestor = new GestorTareas();
        }
        return instanciaGestor;
    }

    /**
     * Estable el {@link Context} utilizado por el {@link SQLiteManager}.
     * Este debe ser establecido antes de poder acceder a la base de datos.
     * @param ctx El {@link Context} a utilizar.
     */
    public void setManager(Context ctx) {
        sqlManager = new SQLiteManager(ctx, SQLiteManager.NOMBRE_MANAGER_SQLITE, null, 1);
    }

    public boolean isAplicarFiltro() {
        return this.aplicarFiltro;
    }

    public TipoVista getTipoVista() {
        return ultimaVistaAccedida;
    }

    public void setTipoVista(TipoVista ultimaVista) {
        this.ultimaVistaAccedida = ultimaVista;
    }

    public void setFiltros(List<Filtro> filtros) {
        this.filtros = filtros;
        this.aplicarFiltro = true;
    }

    /**
     * Obtiene todas las tareas de la aplicación.
     * @return Una {@link List} con las {@link Tarea} obtenidas. Puede estar vacía.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public List<Tarea> getTareas() throws ParseException {
        checkNullManager();

        return sqlManager.getTareas();
    }

    /**
     * Obtiene una tarea por su id.
     * @param idTarea El id de la tarea a obtener.
     * @return La {@link Tarea} obtenida.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public Tarea getTarea(int idTarea) throws ParseException {
        checkNullManager();

        return sqlManager.getTareaById(idTarea);
    }

    /**
     * Obtiene todas las tareas de la aplicación con una fecha igual a la establecida.
     * @param fecha La fecha de las tareas a obtener.
     * @return Una {@link List} con las {@link Tarea} obtenidas. Puede estar vacía.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public List<Tarea> getTareas(Calendar fecha) throws ParseException {
        checkNullManager();
        // Solo año, mes y día; no tiene en cuenta la hora y minutos
        DateFormat formatoLike = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String fechaLike = formatoLike.format(fecha.getTime());

        return sqlManager.getTareasLikeFecha(fechaLike);
    }

    /**
     * Crea una nueva tarea en la aplicación.
     * @param nuevaTarea La nueva {@link Tarea} a ser creada.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public void crearTarea(Tarea nuevaTarea) {
        checkNullManager();
        // Establece los segundos a cero; no son necesarios
        nuevaTarea.getFechaFin().set(Calendar.SECOND, 0);
        sqlManager.createTarea(nuevaTarea);
    }

    /**
     * Borra una tarea de la aplicación.
     * @param idTarea La  {@link Tarea} a ser eliminada.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public void borrarTarea(int idTarea) {
        checkNullManager();

        sqlManager.removeTarea(idTarea);
    }

    /**
     * Edita una tarea ya existente.
     * @param idTarea El id de la tarea a editar.
     * @param nuevosDatosTarea Los nuevos datos de la tarea a editar.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public void editarTarea(int idTarea, Tarea nuevosDatosTarea)  {
        checkNullManager();
        // Establece los segundos a cero; no son necesarios
        nuevosDatosTarea.getFechaFin().set(Calendar.SECOND, 0);
        sqlManager.updateTarea(idTarea, nuevosDatosTarea);
    }

    /**
     * Establece una tarea como completada.
     * @param idTarea El id de la tarea a completar.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public void completarTarea(int idTarea) {
        checkNullManager();

        sqlManager.updateTareaEstado(idTarea, Estado.COMPLETADO);
    }

    /**
     * Actualiza el estado de las tareas sin completar ya vencidas a VENCIDO.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public void actualizarEstadoTareas() {
        checkNullManager();
        Calendar fechaAhora = Calendar.getInstance();
        // Establece los segundos a cero; no son necesarios
        fechaAhora.set(Calendar.SECOND, 0);
        sqlManager.updateTareasEstado(fechaAhora);
    }

    /**
     * Notifica todas las tareas cuyo estado puede ser actualizado (es decir, las que van a
     * ser vencidas)
     * @param ctx El {@link Context} de la aplicación.
     * @param classObjetivo La {@link Class} de la actividad que llama a este método.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public void notificarTareasActualizables(Context ctx, Class classObjetivo) throws ParseException {
        Calendar fechaAhora = Calendar.getInstance();
        // Establece los segundos a cero; no son necesarios
        fechaAhora.set(Calendar.SECOND, 0);
        for (Tarea tarea: getTareasActualizables(fechaAhora)) {
            enviarNotificacionTareaVencida(ctx, classObjetivo, ctx.getResources().getString(R.string.notificacion_vencida_titulo),
                    String.format(ctx.getResources().getString(R.string.notificacion_vencida_contenido), tarea.getNombre(),
                            Tarea.calendarToPrettyString(tarea.getFechaFin())));
        }
    }

    /**
     * Obtiene las tareas que pueden ser actualizables. Es decir, las tareas sin completar cuyo
     * estado puede cambiar a vencido.
     * @param fechaAhora La fecha utilizada para comprobar si las tareas han vencido.
     * @return La {@link List} de {@link Tarea} obtenidas. Puede estar vacía.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    private List<Tarea> getTareasActualizables(Calendar fechaAhora) throws ParseException {
        checkNullManager();

        return sqlManager.getTareasByFechaAndEstado(fechaAhora, Estado.SIN_COMPLETAR);
    }

    /**
     * Obtiene todas las tareas de la aplicación, filtradas con el filtro establecido.
     * @return Una {@link List} de {@link Tarea} que cumplen los filtros establecidos. Puede estar vacía.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado o los filtros no han sido inicializados.
     * @throws ParseException Cuando ocurre un error al convertir la fecha de la BD.
     */
    public List<Tarea> filtrarTareas() throws ParseException {
        if (filtros == null) {
            throw new IllegalStateException("Filtros no inicializados");
        }
        List<Tarea> tareas = this.getTareas();
        for (Filtro filtro : filtros) {
            tareas = filtro.filtrarTareas(tareas);
        }

        // Desactivar aplicación de filtros; ya han sido aplicados
        aplicarFiltro = false;
        return tareas;
    }

    /**
     * Obtiene todas las categorías de la aplicación.
     * @return Una {@link List} de las {@link Categoria} obtenidas. Puede estar vacía.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public List<Categoria> getCategorias() {
        checkNullManager();

        return sqlManager.getCategorias();
    }

    /**
     * Obtiene una categoría por su id.
     * @param idCategoria El id de la categoría a obtener.
     * @return La {@link Categoria} obtenida.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public Categoria getCategoria(int idCategoria) {
        checkNullManager();

        return sqlManager.getCategoriaById(idCategoria);
    }

    /**
     * Obtiene una categoría por su nombre.
     * @param nombre El nombre de la categoría a obtener.
     * @return La {@link Categoria} obtenida.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public Categoria getCategoria(String nombre) {
        checkNullManager();

        return sqlManager.getCategoriaByName(nombre);
    }

    /**
     * Crea una nueva categoría en la aplicación.
     * @param nuevaCategoria La nueva {@link Categoria} a ser creada.
     * @throws IllegalStateException Lanzado cuando el {@link SQLiteManager} no ha sido inicializado.
     */
    public void crearCategoria(Categoria nuevaCategoria) {
        checkNullManager();

        sqlManager.createCategoria(nuevaCategoria);
    }

    /**
     * Importa las tareas desde un archivo XML.
     * @param archivoXML La ruta del archivo XML desde el que se importarán las tareas.
     * @throws Exception Lanzada cuando ocurre un error al acceder o procesar el archivo XML y las tareas.
     */
    public void importarTareas(File archivoXML) throws Exception {
        Gestor gestor = xmlManager.parseXML(archivoXML);
        List<CategoriaXML> categoriasXML = gestor.getCategorias();
        List<Categoria> categorias = new ArrayList<>();
        if (categoriasXML != null) {
            categorias.addAll(CategoriaXML.convertirCategoriasXML(categoriasXML));
        }
        List<TareaXML> tareasXML = gestor.getTareas();

        // Para cada categoria del archivo XML: si esta no existe, crearla.
        for (Categoria categoria : categorias) {
            if (categoria.getNombre() != null && getCategoria(categoria.getNombre()) == null) {
                crearCategoria(categoria);
            }
        }

        // Para cada tarea del archivo XML: si su categoría no es null, buscar la categoría pertinente y establecerla.
        // Después, añadir la tarea.
        if (tareasXML != null) {
            for (TareaXML tareaXML : tareasXML) {
                Tarea tarea = TareaXML.convertirTarea(tareaXML);
                if (tareaXML.getNombreCategoria() != null) {
                    tarea.setCategoria(getCategoria(tareaXML.getNombreCategoria()));
                }
                crearTarea(tarea);
            }
        }
    }

    /**
     * Exporta las tareas de la aplicación a un archivo XML.
     * @param archivoXML La ruta del archivo XML a ser escrito.
     * @throws Exception Lanzada cuando ocurre un error al acceder o procesar el archivo XML y las tareas.
     */
    public void exportarTareas(String archivoXML) throws Exception {
        List<Tarea> tareas = getTareas();
        List<Categoria> categorias = getCategorias();
        Gestor gestor = new Gestor(Gestor.VERSION_ACTUAL, TareaXML.convertirTareasBD(tareas),
                CategoriaXML.convertirCategoriasBD(categorias));
        xmlManager.crearXML(archivoXML, gestor);
    }

    /**
     * Envía una notificación desde el manager de notificaciones.
     * @param ctx El {@link Context} a utilizar.
     * @param classObjetivo La {@link Class} de la actividad que llama a este método.
     * @param titulo El título de la notificación.
     * @param contenido El contenido de la notificación.
     */
    private void enviarNotificacionTareaVencida(Context ctx, Class classObjetivo, String titulo, String contenido) {
        notificationManager.enviarNotificacion(ctx, classObjetivo, ManagerNotificaciones.DEFAULT_CHANNEL_ID,
                R.drawable.file_text_open_source, titulo, contenido);
    }

    /**
     * Usado para comprobar que el manager SQL está inicializado.
     */
    private void checkNullManager() {
        if (sqlManager == null) {
            throw new IllegalStateException("SQLiteManager no inicializado.");
        }
    }




}
