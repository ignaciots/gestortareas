package org.ignaciots.gestortareas.logica.notificacion;

import androidx.annotation.NonNull;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import org.ignaciots.gestortareas.MainActivity;
import org.ignaciots.gestortareas.logica.GestorTareas;

import java.text.ParseException;

/**
 * Clase que se encarga de realizar notificaciones y actualizar la base de datos en segundo plano.
 * @author Ignacio Torre Suáres
 */
public class ServicioNotificaciones extends Service {

    // nombre del hilo del servicio
    private static final String NOMBRE_HILO_HANDLER = "HiloServicioNotificaciones";

    private Looper looper;
    private ServicioNotificacionesHandler handler;

    @Override
    public void onCreate() {
        // Iniciar el hilo
        HandlerThread thread = new HandlerThread(NOMBRE_HILO_HANDLER,
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Establecer el handler
        looper = thread.getLooper();
        handler = new ServicioNotificacionesHandler(looper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Ejecutar el handler
        Message message = handler.obtainMessage();
        message.arg1 = startId;
        handler.sendMessage(message);

        // Reiniciar el servicio si este es terminado
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * {@link Handler} para {@link ServicioNotificaciones} que se encargará de ejecutar
     * el código de envío de notificaciones y actualización de la base de datos.
     */
    private final class ServicioNotificacionesHandler extends Handler {

        ServicioNotificacionesHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            boolean errorObtenerTareas = false;
            GestorTareas gestor = GestorTareas.getInstance();
            gestor.setManager(ServicioNotificaciones.this);
            try {
                gestor.notificarTareasActualizables(ServicioNotificaciones.this, MainActivity.class);
            } catch (ParseException e) {
                errorObtenerTareas = true;
            }
            if (errorObtenerTareas) return;

            gestor.actualizarEstadoTareas();


            // Finalizar el servicio al terminar el handler
            stopSelf(msg.arg1);
        }
    }

}
