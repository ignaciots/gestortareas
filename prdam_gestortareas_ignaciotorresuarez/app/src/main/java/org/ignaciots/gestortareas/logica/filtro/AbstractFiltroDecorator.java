package org.ignaciots.gestortareas.logica.filtro;

import org.ignaciots.gestortareas.model.Tarea;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase abstract que implementa funcionalidad básica común de los filtros que siguen el patrón decorator.
 * @author Ignacio Torre Suárez
 */
public abstract class AbstractFiltroDecorator extends AbstractFiltro {

    protected Filtro filtro;

    protected AbstractFiltroDecorator(Filtro filtro) {
        this.filtro = filtro;
    }

    @Override
    public List<Tarea> filtrarTareas(List<Tarea> tareas) {
        // Filtra las tareas con el filtro pasado por el constructor.
        // Después, usa el estado resultante de ese filtro para realizar el propio filtrado del filtro
        // AbstractFiltroDecorator.
        List<Tarea> tareasFiltrar = new ArrayList<>();
        filtro.filtrarTareas(tareas);
        int[] estado = filtro.getEstadoFiltro();
        this.estadoFiltroTareas = estado;
        int i = 0;
        for (Tarea tarea : tareas) {
            estado[i] = doFiltrarEstado(estado[i]);
            if (estado[i] == 0) {
                tareasFiltrar.add(tarea);
            }
            i++;
        }
        return tareasFiltrar;
    }

    @Override
    protected int comparar(Tarea tarea) {
        return 0;
    }

    /**
     * Filtra un estado y devuelve otro estado.
     * @param estado El estado a filtrar.
     * @return El estado resultante.
     */
    protected abstract int doFiltrarEstado(int estado);
}
