package org.ignaciots.gestortareas.logica.filtro;

import org.ignaciots.gestortareas.model.Tarea;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase abstract que implementa funcionalidad básica común de todos los filtros.
 * @author Ignacio Torre Suárez
 */
public abstract class AbstractFiltro implements Filtro {

    protected int[] estadoFiltroTareas;

    protected AbstractFiltro() {}

    @Override
    public List<Tarea> filtrarTareas(List<Tarea> tareas) {
        // Aplica los filtros y establece el estado resultante del filtro
        List<Tarea> tareasFiltradas = new ArrayList<>();
        this.estadoFiltroTareas = new int[tareas.size()];
        int i = 0;
        for (Tarea tarea : tareas) {
            if ((estadoFiltroTareas[i] = comparar(tarea)) == 0) {
                tareasFiltradas.add(tarea);
            }
            i++;
        }
        return tareasFiltradas;
    }

    @Override
    public int[] getEstadoFiltro() {
        return estadoFiltroTareas;
    }

    /**
     * Compara una tarea con otra. Devuelve 0 sin so iguales, 1 si es mayor o -1 si es menor.
     * @param tarea La tarea a comprar.
     * @return int que indica el resultado de la comparación.
     */
    protected abstract int comparar(Tarea tarea);
}
