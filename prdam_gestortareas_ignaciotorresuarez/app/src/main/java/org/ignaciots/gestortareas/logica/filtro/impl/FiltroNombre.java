package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroTexto;
import org.ignaciots.gestortareas.model.Tarea;

/**
 * Filtro utilizado para filtrar nombres.
 * @author Ignacio Torre Suárez
 */
public class FiltroNombre extends AbstractFiltroTexto {

    /**
     * Crea un filtro que filtra por nombres que contienen el texto dado.
     * @param texto El texto usado en el filtro.
     */
    public FiltroNombre(String texto) {
        super(texto);
    }

    @Override
    protected String getTextoTarea(Tarea tarea) {
        return tarea.getNombre();
    }
}
