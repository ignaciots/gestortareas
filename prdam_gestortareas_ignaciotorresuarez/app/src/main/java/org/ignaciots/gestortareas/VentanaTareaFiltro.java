package org.ignaciots.gestortareas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.ignaciots.gestortareas.logica.GestorTareas;
import org.ignaciots.gestortareas.logica.filtro.Filtro;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroCategoria;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroCualquiera;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroDescripcion;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroEstado;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroFecha;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroHora;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroMayorQue;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroMenorQue;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNombre;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNot;
import org.ignaciots.gestortareas.logica.filtro.impl.FiltroNotificable;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Activity que permite filtrar tareas.
 * Permite filtrar por su nombre, descripción, fecha, categoría, estado y configuración de notificaciones.
 *
 * @author  Ignacio Torre Suárez
 */
public class VentanaTareaFiltro extends AppCompatActivity {

    private Spinner spinnerFiltroNombre;
    private Spinner spinnerFiltroDescripcion;
    private Spinner spinnerFiltroFecha;
    private Spinner spinnerFiltroHora;
    private Spinner spinnerFiltroCategoria;
    private Spinner spinnerFiltroEstado;
    private Spinner spinnerFiltroNotificable;

    private EditText editTextNombre;
    private EditText editTextDescripcion;
    private TextView textViewFecha;
    private TextView textViewHora;
    private EditText editTextCategoria;
    private Spinner spinnerEstado;
    private Spinner spinnerNotificable;
    private Button buttonCambiarFecha;
    private Button buttonCambiarHora;
    private Button buttonFiltrar;
    private Button buttonCancelar;

    private GestorTareas gestor;
    private FiltroEstadoEnum estadoSeleccionado = null;
    private FiltroNotificableEnum notificableSeleccionado = null;
    private Integer anyoSeleccionado = null;
    private Integer mesSeleccionado = null;
    private Integer diaSeleccionado = null;
    private Integer horaSeleccionada = null;
    private Integer minutoSeleccionado = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_tarea_filtro);
        setTitle(getResources().getText(R.string.filtrar_titulo));

        gestor = GestorTareas.getInstance();
        gestor.setManager(this);
        spinnerFiltroNombre = findViewById(R.id.spinnerFiltroNombre);
        spinnerFiltroDescripcion = findViewById(R.id.spinnerFiltroDescripcion);
        spinnerFiltroFecha = findViewById(R.id.spinnerFiltroFecha);
        spinnerFiltroHora = findViewById(R.id.spinnerFiltroHora);
        spinnerFiltroCategoria = findViewById(R.id.spinnerFiltroCategoria);
        spinnerFiltroEstado = findViewById(R.id.spinnerFiltroEstado);
        spinnerFiltroNotificable = findViewById(R.id.spinnerFiltroNotificable);

        editTextNombre = findViewById(R.id.editTextFiltroNombreDato);
        editTextDescripcion = findViewById(R.id.editTextFiltroDescripcionDato);
        textViewFecha = findViewById(R.id.textViewFiltroFechaFinDato);
        textViewHora = findViewById(R.id.textViewFiltroHoraDato);
        editTextCategoria = findViewById(R.id.editTextFiltroCategoriaDato);
        spinnerEstado = findViewById(R.id.spinnerFiltroEstadoDato);
        spinnerNotificable = findViewById(R.id.spinnerFiltroNotificableDato);
        buttonCambiarFecha = findViewById(R.id.buttonFiltroFechaCambiar);
        buttonCambiarHora = findViewById(R.id.buttonFiltroHoraCambiar);
        buttonFiltrar = findViewById(R.id.buttonFiltroFiltrar);
        buttonCancelar = findViewById(R.id.buttonFiltroCancelar);

        // Inicializa los spinner de filtro
        FiltroEnum[] filtro1 = new FiltroEnum[] {FiltroEnum.IGUAL, FiltroEnum.DISTINTO};
        FiltroEnum[] filtro2 = new FiltroEnum[] {FiltroEnum.IGUAL, FiltroEnum.DISTINTO, FiltroEnum.MAYOR_QUE, FiltroEnum.MENOR_QUE};
        setUpSpinnerFiltro(spinnerFiltroNombre, filtro1);
        setUpSpinnerFiltro(spinnerFiltroDescripcion, filtro1);
        setUpSpinnerFiltro(spinnerFiltroCategoria, filtro1);
        setUpSpinnerFiltro(spinnerFiltroEstado, filtro1);
        setUpSpinnerFiltro(spinnerFiltroNotificable, filtro1);
        setUpSpinnerFiltro(spinnerFiltroFecha, filtro2);
        setUpSpinnerFiltro(spinnerFiltroHora, filtro2);

        setUpSpinnerEstado();
        setUpSpinnerNotificable();

        spinnerEstado.setOnItemSelectedListener(new SpinnerEstadoOnItemSelectedListener());
        spinnerNotificable.setOnItemSelectedListener(new SpinnerNotificableOnItemSelectedListener());
        buttonCambiarFecha.setOnClickListener(new ButtonFechaCambiarOnClickListener());
        buttonCambiarHora.setOnClickListener(new ButtonHoraCambiarOnClickListener());
        buttonFiltrar.setOnClickListener(new ButtonFiltroFiltrarOnClickListener());
        buttonCancelar.setOnClickListener(new ButtonFiltroCancelarOnClickListener());
    }

    /**
     * Inicializa un spinner con los filtros dados.
     * @param spinner El {@link Spinner} a inicializar.
     * @param filtros Los {@link FiltroEnum} que soportará el spinner.
     */
    private void setUpSpinnerFiltro(Spinner spinner, FiltroEnum[] filtros) {
        ArrayAdapter<FiltroEnum> adapterSpinnerFiltro = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, filtros);
        adapterSpinnerFiltro.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinnerFiltro);
    }

    /**
     * Inicializa el spinner que muestra todos los estados posibles de una tarea.
     */
    private void setUpSpinnerEstado() {
        List<FiltroEstadoEnum> estados = new ArrayList<>(Arrays.asList(FiltroEstadoEnum.values()));

        ArrayAdapter<FiltroEstadoEnum> adapterSpinnerEstado = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, estados);
        adapterSpinnerEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstado.setAdapter(adapterSpinnerEstado);
    }

    /**
     * Inicializa el spinner que muestra todos los tipos de notificación posibles de una tarea.
     */
    private void setUpSpinnerNotificable() {
        List<FiltroNotificableEnum> estadosNotificacion = new ArrayList<>(Arrays.asList(FiltroNotificableEnum.values()));

        ArrayAdapter<FiltroNotificableEnum> adapterSpinnerNotificable = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, estadosNotificacion);
        adapterSpinnerNotificable.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNotificable.setAdapter(adapterSpinnerNotificable);
    }

    /**
     * Enumerado que gestiona los posibles filtros de un spinner.
     */
    private enum FiltroEnum {

        IGUAL("="),
        DISTINTO("≠"),
        MAYOR_QUE(">"),
        MENOR_QUE("<");

        private final String nombre;

        FiltroEnum(String nombre) {
            this.nombre = nombre;
        }

        String getNombre() {
            return this.nombre;
        }

        @Override
        @NonNull
        public String toString() {
            return getNombre();
        }
    }

    /**
     * Enumerado que gestiona los posibles valores del spinner de estado
     */
    private enum FiltroEstadoEnum {
        CUALQUIERA(null),
        SIN_COMPLETAR(Estado.SIN_COMPLETAR),
        COMPLETADO(Estado.COMPLETADO),
        VENCIDO(Estado.VENCIDO);

        private final Estado estado;

        FiltroEstadoEnum(Estado estado) {
            this.estado = estado;
        }

        Estado getEstado() {
            return this.estado;
        }

        @NonNull
        @Override
        public String toString() {
            if (estado  == null) {
                return "Cualquiera";
            } else {
                return estado.toString();
            }
        }
    }

    /**
     * Enumerado que gestiona los posibles valores del spinner de notificaciones
     */
    private enum FiltroNotificableEnum {
        CUALQUIERA(null),
        NOTIFICABLE(Notificable.NOTIFICABLE),
        NO_NOTIFICABLE(Notificable.NO_NOTIFICABLE);

        private final Notificable notificable;

        FiltroNotificableEnum(Notificable notificable) {
            this.notificable = notificable;
        }

        Notificable getNotificable() {
            return this.notificable;
        }

        @NonNull
        @Override
        public String toString() {
            if (notificable  == null) {
                return "Cualquiera";
            } else {
                return notificable.toString();
            }
        }
    }

    /**
     * Listener utilizada para establecer el estado de la nueva tarea a crear.
     */
    private class SpinnerEstadoOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            estadoSeleccionado = (FiltroEstadoEnum) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    }

    /**
     * Listener utilizada para establecer el tipo de notificación de la nueva tarea a crear.
     */
    private class SpinnerNotificableOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            notificableSeleccionado = (FiltroNotificableEnum) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    }

    /**
     * Listener para el botón de fecha que lanza el diálogo de selección de fecha {@link DatePickerDialog}.
     */
    private class ButtonFechaCambiarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Calendar fechaHoy = Calendar.getInstance();
            int anyo = fechaHoy.get(Calendar.YEAR);
            int mes = fechaHoy.get(Calendar.MONTH);
            int dia = fechaHoy.get(Calendar.DAY_OF_MONTH);

            // Lanzar el diálogo con la fecha actual seleccionada
            DatePickerDialog dialogoFecha = new DatePickerDialog(VentanaTareaFiltro.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Actualizar la fecha seleccionada para crear la nueva tarea
                            VentanaTareaFiltro.this.anyoSeleccionado = year;
                            VentanaTareaFiltro.this.mesSeleccionado = monthOfYear;
                            VentanaTareaFiltro.this.diaSeleccionado = dayOfMonth;
                            Calendar fechaSeleccionada = Calendar.getInstance();
                            fechaSeleccionada.set(anyoSeleccionado, mesSeleccionado, diaSeleccionado);
                            textViewFecha.setText(Tarea.calendarDateToPrettyString(fechaSeleccionada));
                        }
                    }, anyo, mes, dia);
            dialogoFecha.show();
        }
    }

    /**
     * Listener para el botón de fecha que lanza el diálogo de selección de fecha {@link TimePickerDialog}.
     */
    private class ButtonHoraCambiarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Calendar ahora = Calendar.getInstance();
            int hora = ahora.get(Calendar.HOUR_OF_DAY);
            int minuto = ahora.get(Calendar.MINUTE);

            // Lanzar el diálogo con la hora actual seleccionada
            TimePickerDialog dialogoHora = new TimePickerDialog(VentanaTareaFiltro.this,
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            // Actualizar la hora seleccionada para crear la nueva tarea
                            VentanaTareaFiltro.this.horaSeleccionada = hourOfDay;
                            VentanaTareaFiltro.this.minutoSeleccionado = minute;
                            Calendar hora = Calendar.getInstance();
                            hora.set(Calendar.HOUR_OF_DAY, horaSeleccionada);
                            hora.set(Calendar.MINUTE, minutoSeleccionado);
                            textViewHora.setText(Tarea.calendarTimeToPrettyString(hora));
                        }
                    }, hora, minuto, true);
            dialogoHora.show();
        }
    }

    /**
     * Listener que permite aplicar los filtros establecidos al pulsar el botón de filtrar.
     * Aplica los filtros al {@link GestorTareas} para que sean posteriormente ejecutados.
     */
    private class ButtonFiltroFiltrarOnClickListener implements  View.OnClickListener {

        @Override
        public void onClick(View v) {
            List<Filtro> filtros = new ArrayList<>();
            // Añadir los filtros de texto y enumerados
            intentarAddFiltroTexto(filtros, editTextNombre, spinnerFiltroNombre, new FiltroNombre(editTextNombre.getText().toString()));
            intentarAddFiltroTexto(filtros, editTextDescripcion, spinnerFiltroDescripcion, new FiltroDescripcion(editTextDescripcion.getText().toString()));
            intentarAddFiltroTexto(filtros, editTextCategoria, spinnerFiltroCategoria, new FiltroCategoria(editTextCategoria.getText().toString()));
            intentarAddFiltroEstado(filtros, estadoSeleccionado, spinnerFiltroEstado, new FiltroEstado(estadoSeleccionado.getEstado()));
            intentarAddFiltroNotificable(filtros, notificableSeleccionado, spinnerFiltroNotificable, new FiltroNotificable(notificableSeleccionado.getNotificable()));

            // Añadir los filtros de fecha y hora
            if (anyoSeleccionado != null) {
                Calendar fechaSeleccionada = Calendar.getInstance();
                fechaSeleccionada.set(anyoSeleccionado, mesSeleccionado, diaSeleccionado, 0, 0, 0);
                addFiltroFechaHora(filtros, spinnerFiltroFecha, new FiltroFecha(fechaSeleccionada));
            }
            if (horaSeleccionada != null) {
                Calendar tiempoSeleccionado = Calendar.getInstance();
                tiempoSeleccionado.set(Calendar.HOUR_OF_DAY, horaSeleccionada);
                tiempoSeleccionado.set(Calendar.MINUTE, minutoSeleccionado);
                tiempoSeleccionado.set(Calendar.SECOND, 0);
                addFiltroFechaHora(filtros, spinnerFiltroHora, new FiltroHora(tiempoSeleccionado));
            }

            gestor.setFiltros(filtros);
            finish();
            Toast.makeText(VentanaTareaFiltro.this, VentanaTareaFiltro.this.getResources().getText(R.string.filtro_filtrar_correcto), Toast.LENGTH_SHORT).show();
        }

        /**
         * Añade a la lista de filtros un {@link Filtro} indicado por un {@link Spinner} y otro {@link Filtro} dado.
         * @param filtros La lista de filtros a la que se añadirá el filtro.
         * @param spinnerFiltro El {@link Spinner} utilizado para establecer el filtro.
         * @param filtroFecha El {@link Filtro} de fecha utilizado.
         */
        private void addFiltroFechaHora(List<Filtro> filtros, Spinner spinnerFiltro, Filtro filtroFecha) {
            filtros.add(crearFiltro((FiltroEnum)spinnerFiltro.getSelectedItem(), filtroFecha));
        }

        /**
         * Añade a la lista de filtros un {@link Filtro} indicado por un {@link Spinner} y otro {@link Filtro} dado.
         * @param filtros La lista de filtros a la que se añadirá el filtro.
         * @param estadoSeleccionado El estado utilizado para establecer el filtro.
         * @param spinnerFiltro El {@link Spinner} utilizado para establecer el filtro.
         * @param filtroEstado El {@link Filtro} de estado utilizado.
         */
        private void intentarAddFiltroEstado(List<Filtro> filtros, FiltroEstadoEnum estadoSeleccionado, Spinner spinnerFiltro, Filtro filtroEstado) {
            if (estadoSeleccionado.getEstado() != null) {
                filtros.add(crearFiltro((FiltroEnum)spinnerFiltro.getSelectedItem(), filtroEstado));
            } else {
                // Si el filtro es "Cualquiera", añadir un FiltroCualquiera
                filtros.add(crearFiltro((FiltroEnum)spinnerFiltro.getSelectedItem(), new FiltroCualquiera(filtroEstado)));
            }
        }

        /**
         * Añade a la lista de filtros un {@link Filtro} indicado por un {@link Spinner} y otro {@link Filtro} dado.
         * @param filtros La lista de filtros a la que se añadirá el filtro.
         * @param notificableSeleccionado El estado notificable para establecer el filtro.
         * @param spinnerFiltro El {@link Spinner} utilizado para establecer el filtro.
         * @param filtroNotificable El {@link Filtro} de notificable utilizado.
         */
        private void intentarAddFiltroNotificable(List<Filtro> filtros, FiltroNotificableEnum notificableSeleccionado, Spinner spinnerFiltro, Filtro filtroNotificable) {
            if (notificableSeleccionado.getNotificable() != null) {
                filtros.add(crearFiltro((FiltroEnum)spinnerFiltro.getSelectedItem(), filtroNotificable));
            } else {
                // Si el filtro es "Cualquiera", añadir un FiltroCualquiera
                filtros.add(crearFiltro((FiltroEnum)spinnerFiltro.getSelectedItem(), new FiltroCualquiera(filtroNotificable)));
            }
        }

        /**
         * Añade a la lista de filtros un {@link Filtro} indicado por un {@link Spinner} y otro {@link Filtro} dado.
         * @param filtros La lista de filtros a la que se añadirá el filtro.
         * @param editText El texto utilizado para establecer el filtro.
         * @param spinnerFiltro El {@link Spinner} utilizado para establecer el filtro.
         * @param filtro El {@link Filtro} de texto utilizado.
         */
        private void intentarAddFiltroTexto(List<Filtro> filtros, EditText editText, Spinner spinnerFiltro, Filtro filtro) {
            String texto = editText.getText().toString();
            if (!texto.trim().isEmpty()) {
                filtros.add(crearFiltro((FiltroEnum)spinnerFiltro.getSelectedItem(), filtro));
            }
        }

        /**
         * Crea un filtro decorator a partir de un {@link Filtro} y un {@link FiltroEnum}
         * @param filtroEnum el {@link FiltroEnum} utilizado para establecer el filtro.
         * @param filtro El {@link Filtro} utilizado en el {@link org.ignaciots.gestortareas.logica.filtro.AbstractFiltroDecorator}.
         * @return El {@link Filtro} creado.
         */
        private Filtro crearFiltro(FiltroEnum filtroEnum, Filtro filtro) {
            switch (filtroEnum) {
                default:
                case IGUAL:
                    return filtro;
                case DISTINTO:
                    return new FiltroNot(filtro);
                case MAYOR_QUE:
                    return new FiltroMayorQue(filtro);
                case MENOR_QUE:
                    return new FiltroMenorQue(filtro);
            }
        }
    }

    /**
     * Listener utilizado para el botón de cancelar.
     */
    private class ButtonFiltroCancelarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            finish();
        }
    }
}
