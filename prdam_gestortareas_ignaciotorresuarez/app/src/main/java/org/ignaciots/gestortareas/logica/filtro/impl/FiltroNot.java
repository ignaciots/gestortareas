package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroDecorator;
import org.ignaciots.gestortareas.logica.filtro.Filtro;

/**
 * Filtro decorator utilizado para filtrar tareas cuyo resultado del filtro haya sido distonto de 0.
 * @author Ignacio Torre Suárez
 */
public class FiltroNot extends AbstractFiltroDecorator {

    /**
     * Crea un filtro que filtra por resultados distintos a 0 del filtro pasado como parámetro.
     * @param filtro El filtro a ejecutar.
     */
    public FiltroNot(Filtro filtro) {
        super(filtro);
    }

    @Override
    protected int doFiltrarEstado(int estado) {
        return estado != 0 ? 0 : 1;
    }
}
