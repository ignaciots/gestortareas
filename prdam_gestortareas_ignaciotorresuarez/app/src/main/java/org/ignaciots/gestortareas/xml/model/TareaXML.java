package org.ignaciots.gestortareas.xml.model;

import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;
import org.simpleframework.xml.Attribute;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que representa a la entidad Tarea utilizada en el modelo XML.
 * @author Ignacio Torre Suárez
 */
public class TareaXML {

    @Attribute
    private String nombre;

    @Attribute(required = false)
    private String descripcion;

    @Attribute
    private String fecha;

    @Attribute
    private Estado estado;

    @Attribute
    private Notificable notificable;

    @Attribute(required = false)
    private String nombreCategoria;

    public TareaXML() {}

    public TareaXML(String nombre, String descripcion, String fecha, Estado estado, Notificable notificable, String nombreCategoria) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.estado = estado;
        this.notificable = notificable;
        this.nombreCategoria = nombreCategoria;
    }

    /**
     * Convierte una Tarea del modelo de bases de datos a una Tarea del modelo XML.
     * @param tareaDB La {@link Tarea} del modelo de bases de datos.
     * @return La {@link TareaXML} del modelo XML equivalente.
     */
    public static TareaXML convertirTarea(Tarea tareaDB) {
        return new TareaXML(tareaDB.getNombre(), tareaDB.getDescripcion(),
                Tarea.calendarToString(tareaDB.getFechaFin()),
                tareaDB.getEstado(), tareaDB.getNotificable(), tareaDB.getCategoria() == null ? null : tareaDB.getCategoria().getNombre());
    }

    /**
     * Convierte una lista de Tareas del modelo de bases de datos a una lista de Tareas del modelo XML.
     * @param tareasDB  La {@link List} de {@link Tarea} del modelo de bases de datos.
     * @return La {@link List} de {@link TareaXML} del modelo XML equivalente.
     */
    public static List<TareaXML> convertirTareasBD(List<Tarea> tareasDB) {
        List<TareaXML> tareas = new ArrayList<>();
        if (tareasDB != null) {
            for (Tarea tarea : tareasDB) {
                tareas.add(convertirTarea(tarea));
            }
        }

        return tareas;
    }

    /**
     * Convierte una Tarea del modelo XML a una Tarea del modelo de bases de datos.
     * @param tareaXML La {@link TareaXML} del modelo XML.
     * @return La {@link Tarea} del modelo de bases de datos equivalente.
     */
    public static Tarea convertirTarea(TareaXML tareaXML) throws ParseException {
        return new Tarea(0, tareaXML.getNombre(), tareaXML.getDescripcion(), Tarea.stringToCalendar(tareaXML.getFecha()),
                tareaXML.getEstado(), tareaXML.getNotificable(), null);
    }

    /**
     * Convierte una lista de Tareas del modelo XML a una lista de Tareas del modelo de bases de datos.
     * @param tareasXML  La {@link List} de {@link TareaXML} del modelo XML.
     * @return La {@link List} de {@link Tarea} del modelo de bases de datos.
     */
    public static List<Tarea> convertirTareasXML(List<TareaXML> tareasXML) throws ParseException {
        List<Tarea> tareas = new ArrayList<>();
        for (TareaXML tareaXML : tareasXML) {
            tareas.add(convertirTarea(tareaXML));
        }

        return tareas;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public Estado getEstado() {
        return estado;
    }

    public Notificable getNotificable() {
        return notificable;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }
}
