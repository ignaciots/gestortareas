package org.ignaciots.gestortareas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.ignaciots.gestortareas.logica.GestorTareas;
import org.ignaciots.gestortareas.model.Categoria;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Notificable;
import org.ignaciots.gestortareas.model.Tarea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Actividad de nueva tarea del gestor de tareas.
 * Permite crear una nueva tarea, estableciendo su nombre, descripción, fecha y hora, categoría, estado
 * y notificable.
 * Pulsando en el botón crear se crea la tarea.
 *
 * @author  Ignacio Torre Suárez
 */
public class VentanaTareaNueva extends AppCompatActivity {

    private EditText editTextNombre;
    private EditText editTextDescripcion;
    private Button btnFechaCambiar;
    private Button btnHoraCambiar;
    private TextView textViewFecha;
    private TextView textViewHora;
    private EditText editTextCategoria;
    private Spinner spinnerEstado;
    private CheckBox checkBoxNotificar;
    private Button btnCrear;
    private Button btnCancelar;

    private GestorTareas gestor;
    private Integer anyoSeleccionado = null;
    private Integer mesSeleccionado = null;
    private Integer diaSeleccionado = null;
    private Integer horaSeleccionada = null;
    private Integer minutoSeleccionado = null;
    private Estado estadoSeleccionado = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_tarea_nueva);
        setTitle(getResources().getText(R.string.nueva_titulo));

        // Inicializar el gestor de tareas y las vistas
        gestor = GestorTareas.getInstance();
        gestor.setManager(this);
        editTextNombre = findViewById(R.id.editTextNuevaNombreDato);
        editTextDescripcion = findViewById(R.id.editTextNuevaDescripcionDato);
        btnFechaCambiar = findViewById(R.id.buttoNuevaFechaCambiar);
        btnHoraCambiar = findViewById(R.id.buttoNuevaHoraCambiar);
        textViewFecha = findViewById(R.id.textViewNuevaFechaFinDato);
        textViewHora = findViewById(R.id.textViewNuevaHoraDato);
        editTextCategoria = findViewById(R.id.editTextNuevaCategoriaDato);
        spinnerEstado = findViewById(R.id.spinnerNuevaEstadoDato);
        checkBoxNotificar = findViewById(R.id.checkBoxNuevaNotificarDato);
        btnCrear = findViewById(R.id.buttonNuevaCrear);
        btnCancelar = findViewById(R.id.buttonNuevaCancelar);

        btnFechaCambiar.setOnClickListener(new ButtonFechaCambiarOnClickListener());
        btnHoraCambiar.setOnClickListener(new ButtonHoraCambiarOnClickListener());
        spinnerEstado.setOnItemSelectedListener(new SpinnerEstadoOnItemSelectedListener());
        btnCrear.setOnClickListener(new ButtonCrearOnClickListener());
        btnCancelar.setOnClickListener(new ButtonCancelarOnClickListener());

        setUpSpinnerEstado();
    }

    /**
     * Inicializa el spinner que muestra todos los estados posibles de una tarea.
     */
    private void setUpSpinnerEstado() {
        List<Estado> estados = new ArrayList<>(Arrays.asList(Estado.values()));

        ArrayAdapter<Estado> adapterSpinnerEstado = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, estados);
        adapterSpinnerEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstado.setAdapter(adapterSpinnerEstado);
    }

    /**
     * Listener para el botón de fecha que lanza el diálogo de selección de fecha {@link DatePickerDialog}.
     */
    private class ButtonFechaCambiarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Calendar fechaHoy = Calendar.getInstance();
            int anyo = fechaHoy.get(Calendar.YEAR);
            int mes = fechaHoy.get(Calendar.MONTH);
            int dia = fechaHoy.get(Calendar.DAY_OF_MONTH);

            // Lanzar el diálogo con la fecha actual seleccionada
            DatePickerDialog dialogoFecha = new DatePickerDialog(VentanaTareaNueva.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Actualizar la fecha seleccionada para crear la nueva tarea
                            VentanaTareaNueva.this.anyoSeleccionado = year;
                            VentanaTareaNueva.this.mesSeleccionado = monthOfYear;
                            VentanaTareaNueva.this.diaSeleccionado = dayOfMonth;
                            Calendar fechaSeleccionada = Calendar.getInstance();
                            fechaSeleccionada.set(anyoSeleccionado, mesSeleccionado, diaSeleccionado);
                            textViewFecha.setText(Tarea.calendarDateToPrettyString(fechaSeleccionada));
                        }
                    }, anyo, mes, dia);
            dialogoFecha.show();
        }
    }

    /**
     * Listener para el botón de fecha que lanza el diálogo de selección de fecha {@link TimePickerDialog}.
     */
    private class ButtonHoraCambiarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Calendar ahora = Calendar.getInstance();
            int hora = ahora.get(Calendar.HOUR_OF_DAY);
            int minuto = ahora.get(Calendar.MINUTE);

            // Lanzar el diálogo con la hora actual seleccionada
            TimePickerDialog dialogoHora = new TimePickerDialog(VentanaTareaNueva.this,
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            // Actualizar la hora seleccionada para crear la nueva tarea
                            VentanaTareaNueva.this.horaSeleccionada = hourOfDay;
                            VentanaTareaNueva.this.minutoSeleccionado = minute;
                            Calendar hora = Calendar.getInstance();
                            hora.set(Calendar.HOUR_OF_DAY, horaSeleccionada);
                            hora.set(Calendar.MINUTE, minutoSeleccionado);
                            textViewHora.setText(Tarea.calendarTimeToPrettyString(hora));
                        }
                    }, hora, minuto, true);
            dialogoHora.show();
        }
    }

    /**
     * Listener utilizada para establecer el estado de la nueva tarea a crear.
     */
    private class SpinnerEstadoOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            estadoSeleccionado = (Estado) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    }

    /**
     * Listener para el botón de crear tarea que crea una nueva tarea y sale de la actividad.
     */
    private class ButtonCrearOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String nombre = editTextNombre.getText().toString();
            String descripcion = editTextDescripcion.getText().toString();
            String categoria = editTextCategoria.getText().toString();

            // Comprobar campos obligatorios
            if (nombre.trim().isEmpty() || anyoSeleccionado == null
               || horaSeleccionada == null || estadoSeleccionado == null) {
                Toast.makeText(VentanaTareaNueva.this, VentanaTareaNueva.this.getResources().getText(R.string.tarea_campos_obligatorios), Toast.LENGTH_LONG).show();
            } else {
                Tarea tarea = new Tarea();
                if (!categoria.trim().isEmpty()) {
                    // Añadir tarea si no existe
                    Categoria categoriaExistente = gestor.getCategoria(categoria);
                    if (categoriaExistente == null) {
                        categoriaExistente = new Categoria();
                        categoriaExistente.setNombre(categoria);
                        gestor.crearCategoria(categoriaExistente);
                        tarea.setCategoria(gestor.getCategoria(categoria));
                    } else {
                        tarea.setCategoria(categoriaExistente);
                    }
                } else {
                    tarea.setCategoria(null);
                }


                tarea.setNombre(nombre);
                tarea.setDescripcion(descripcion.trim().isEmpty() ? null : descripcion);
                Calendar calendario = Calendar.getInstance();
                calendario.set(anyoSeleccionado, mesSeleccionado, diaSeleccionado, horaSeleccionada, minutoSeleccionado);
                tarea.setFechaFin(calendario);
                tarea.setEstado(estadoSeleccionado);
                if (checkBoxNotificar.isChecked()) {
                    tarea.setNotificable(Notificable.NOTIFICABLE);
                } else {
                    tarea.setNotificable(Notificable.NO_NOTIFICABLE);
                }
                gestor.crearTarea(tarea);

                finish();
            }
        }
    }

    /**
     * Listener para el botón que sale de la actividad.
     */
    private class ButtonCancelarOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            finish();
        }
    }
}
