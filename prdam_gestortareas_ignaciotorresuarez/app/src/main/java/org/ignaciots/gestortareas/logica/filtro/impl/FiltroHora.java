package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltro;
import org.ignaciots.gestortareas.model.Tarea;

import java.util.Calendar;

/**
 * Filtro utilizado para filtrar horas de {@link Calendar}.
 * @author Ignacio Torre Suárez
 */
public class FiltroHora extends AbstractFiltro {

    private final Calendar fecha;

    /**
     * Crea un filtro que filtra por horas iguales a la hora dada.
     * @param fecha La hora usado en el filtro.
     */
    public FiltroHora(Calendar fecha) {
        super();
        this.fecha = fecha;
    }

    @Override
    protected int comparar(Tarea tarea) {
        int hora1 = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto1 = fecha.get(Calendar.MINUTE);
        int hora2 = tarea.getFechaFin().get(Calendar.HOUR_OF_DAY);
        int minuto2 = tarea.getFechaFin().get(Calendar.MINUTE);

        if (hora1 > hora2) {
            return -1;
        } else if (hora1 < hora2) {
            return 1;
        } else {
            //noinspection UseCompareMethod
            if (minuto1 > minuto2) {
                return -1;
            } else if (minuto1 < minuto2) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
