package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltro;
import org.ignaciots.gestortareas.model.Tarea;

import java.util.Calendar;

/**
 * Filtro utilizado para filtrar fechas de {@link Calendar}.
 * @author Ignacio Torre Suárez
 */
public class FiltroFecha extends AbstractFiltro {

    private final Calendar fecha;

    /**
     * Crea un filtro que filtra por fechas iguales a la fecha dada.
     * @param fecha La fecha usado en el filtro.
     */
    public FiltroFecha(Calendar fecha) {
        super();
        this.fecha = fecha;
    }

    @Override
    protected int comparar(Tarea tarea) {
        int anyo1 = fecha.get(Calendar.YEAR);
        int mes1 = fecha.get(Calendar.MONTH);
        int dia1 = fecha.get(Calendar.DAY_OF_MONTH);
        int anyo2 = tarea.getFechaFin().get(Calendar.YEAR);
        int mes2 = tarea.getFechaFin().get(Calendar.MONTH);
        int dia2 = tarea.getFechaFin().get(Calendar.DAY_OF_MONTH);

        if (anyo1 > anyo2) {
            return -1;
        } else if (anyo1 < anyo2) {
            return 1;
        } else {
            if (mes1 > mes2) {
                return -1;
            } else if (mes1 < mes2) {
                return 1;
            } else {
                //noinspection UseCompareMethod
                if (dia1 > dia2) {
                    return -1;
                } else if (dia1 < dia2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }
}
