package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltroDecorator;
import org.ignaciots.gestortareas.logica.filtro.Filtro;

/**
 * Filtro decorator utilizado para filtrar tareas cuyo resultado del filtro haya sido 1 (mayor que).
 * @author Ignacio Torre Suárez
 */
public class FiltroMayorQue extends AbstractFiltroDecorator {

    /**
     * Crea un filtro que filtra por resultados mayores a 0 del filtro pasado como parámetro.
     * @param filtro El filtro a ejecutar.
     */
    public FiltroMayorQue(Filtro filtro) {
        super(filtro);
    }

    @Override
    protected int doFiltrarEstado(int estado) {
        return estado > 0 ? 0 : 1;
    }
}
