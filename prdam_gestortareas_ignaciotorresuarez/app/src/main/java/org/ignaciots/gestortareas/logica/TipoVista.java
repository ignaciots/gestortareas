package org.ignaciots.gestortareas.logica;

/**
 * Enumerado que lista los tipos de vista de la aplicación, para su uso por {@link GestorTareas}.
 */
public enum TipoVista {
    /**
     * Vista en modo lista.
     */
    VISTA_LISTA,

    /**
     * Vista en modo calendario.
     */
    VISTA_CALENDARIO
}
