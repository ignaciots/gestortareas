package org.ignaciots.gestortareas.model;

import androidx.annotation.NonNull;

/**
 * Enumerado para el campo estado utilizado en el modelo de base de datos.
 * NO_NOTIFICABLE indica que la tarea no generará notificaciones.
 * NOTIFICABLE indica que la tarea generará notificaciones.
 */
public enum Notificable {
    NO_NOTIFICABLE(0, "No notificar"),
    NOTIFICABLE(1, "Notificar");

    private final int id;
    private final String nombre;

    Notificable(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return this.nombre;
    }

    @Override
    @NonNull
    public String toString() {
        return getNombre();
    }
}
