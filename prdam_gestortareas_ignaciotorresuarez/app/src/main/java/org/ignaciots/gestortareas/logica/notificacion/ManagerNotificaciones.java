package org.ignaciots.gestortareas.logica.notificacion;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

/**
 * Clase que se encarga de gestionar las notificaciones de la aplicación.
 * @author Ignacio Torre Suárez
 */
public class ManagerNotificaciones {

    /**
     * Nombre del canal de notificaciones.
     */
    public static final String NOMBRE_CHANNEL_ID      = "Gestor de tareas";

    /**
     * Descripción del canal de notificaciones.
     */
    public static final String DESCRIPCION_CHANNEL_ID = "Canal del Gestor de tareas";

    /**
     * ID del canal de notificaciones.
     */
    public static final String DEFAULT_CHANNEL_ID     = "org.ignaciots.gestortareas";

    // Contador de ids de notificación
    private static int idNotificacion;

    /**
     * Crea un manager de notificaciones, estableciendo el id de notificación inicial a 0.
     */
    public ManagerNotificaciones() {
        idNotificacion = 0;
    }

    /**
     * Envía una notificación y aumenta el contador global de ids de notificaciones.
     * @param ctx {@link Context} de la aplicación
     * @param classObjetivo {@link Class} de la clase que llama a este método.
     * @param channelId Id del canal de notificaciones.
     * @param drawableIcon Imagen utilizada en la notificación.
     * @param titulo Título de la notificación.
     * @param contenido Texto contenido de la notificación.
     */
    public void enviarNotificacion(Context ctx, Class classObjetivo, String channelId, int drawableIcon,
                                   String titulo, String contenido) {

        // Crear acción al pulsar la notificación (ir a la aplicación)
        Intent intentTap = new Intent(ctx, classObjetivo);
        intentTap.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntentTap = PendingIntent.getActivity(ctx, 0, intentTap, 0);

        // Crear notificación y su configuración
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, channelId);
        builder.setSmallIcon(drawableIcon);
        builder.setContentTitle(titulo);
        builder.setContentText(contenido);
        builder.setContentIntent(pendingIntentTap);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setAutoCancel(true);
        builder.setCategory(NotificationCompat.CATEGORY_REMINDER);
        builder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);

        // Ejecutar notificación y aumentar el contador global de ids de notificaciones
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ctx);
        notificationManager.notify(idNotificacion, builder.build());
        idNotificacion++;
    }


}
