package org.ignaciots.gestortareas.xml.model;

import org.ignaciots.gestortareas.model.Categoria;
import org.simpleframework.xml.Attribute;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que representa a la entidad Categoría utilizada en el modelo XML.
 * @author Ignacio Torre Suárez
 */
public class CategoriaXML {

    @Attribute
    private String nombre;

    public CategoriaXML() {}

    public CategoriaXML(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Convierte una Categoria del modelo de bases de datos a una Categoría del modelo XML.
     * @param categoria La {@link org.ignaciots.gestortareas.model.Categoria} del modelo de bases de datos.
     * @return La {@link CategoriaXML} del modelo XML equivalente.
     */
    public static CategoriaXML convertirCategoria(Categoria categoria) {
        return new CategoriaXML(categoria.getNombre());
    }

    /**
     * Convierte una lista de Categorias del modelo de bases de datos a una lista de Categorías del modelo XML.
     * @param categoriasBD La {@link List} de {@link org.ignaciots.gestortareas.model.Categoria} del modelo de bases de datos.
     * @return La {@link List} de {@link CategoriaXML} del modelo XML equivalente.
     */
    public static List<CategoriaXML> convertirCategoriasBD(List<Categoria> categoriasBD) {
        List<CategoriaXML> categorias = new ArrayList<>();
        if (categoriasBD != null) {
            for (Categoria categoria : categoriasBD) {
                categorias.add(convertirCategoria(categoria));
            }
        }

        return categorias;
    }

    /**
     * Convierte una Categoria del modelo XML a una Categoría del modelo de bases de datos.
     * @param categoriaXML La {@link CategoriaXML}} del modelo XML.
     * @return La {@link Categoria} del modelo de bases de datos equivalente.
     */
    public static Categoria convertirCategoria(CategoriaXML categoriaXML) {
        return new Categoria(0, categoriaXML.getNombre());
    }

    /**
     * Convierte una Categoria del modelo XML a una Categoría del modelo de bases de datos.
     * @param categoriasXML La {@link List} de {@link CategoriaXML}} del modelo XML.
     * @return La {@link List} de {@link Categoria} del modelo de bases de datos equivalente.
     */
    public static List<Categoria> convertirCategoriasXML(List<CategoriaXML> categoriasXML) {
        List<Categoria> categorias = new ArrayList<>();
        for (CategoriaXML categoriaXML : categoriasXML) {
            categorias.add(convertirCategoria(categoriaXML));
        }

        return categorias;
    }

    public String getNombre() {
        return nombre;
    }
}
