package org.ignaciots.gestortareas.logica.filtro.impl;

import org.ignaciots.gestortareas.logica.filtro.AbstractFiltro;
import org.ignaciots.gestortareas.model.Estado;
import org.ignaciots.gestortareas.model.Tarea;

/**
 * Filtro utilizado para filtrar {@link Estado}.
 * @author Ignacio Torre Suárez
 */
public class FiltroEstado extends AbstractFiltro {

    private final Estado estado;

    /**
     * Crea un filtro que filtra por estados iguales al estado dado.
     * @param estado El estado usado en el filtro.
     */
    public FiltroEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    protected int comparar(Tarea tarea) {
        // Usado en filtros
        if (estado == null) {
            return 1;
        }
        return estado.compareTo(tarea.getEstado());
    }
}
